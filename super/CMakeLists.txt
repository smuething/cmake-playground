cmake_minimum_required(VERSION 3.11)

include(CMakePrintHelpers)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/dune-common/cmake/modules")
include(DuneBootstrap)

project(DUNE_MODULE_COLLECTION NAME pdelab)

dune_add_modules(
  DIRECTORIES
    dune-pdelab
    dune-common
    dune-grid
    dune-geometry
  )

dune_finalize()

# set(modules dune-common dune-geometry dune-grid dune-pdelab)

# set_property(GLOBAL PROPERTY DUNE_MODULE_COLLECTION_MODULES VALUE ${modules})

# foreach(mod IN LISTS modules)
#   add_subdirectory(${mod})
# endforeach()

# # dune_finalize_project()

# foreach(mod IN LISTS modules)
#   find_package(${mod} QUIET REQUIRED)
# endforeach()

# feature_summary(WHAT ALL)
