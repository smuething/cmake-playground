include_guard(GLOBAL)

include(DuneParseArguments)

function(dune_define_property)

  dune_parse_function_arguments(
    flags NOT_INHERITED
    single NAME SCOPE DOCS FULL_DOCS
    required NAME DOCS
    )

  if(NOT ARG_NOT_INHERITED)
    set(inherited INHERITED)
  endif()

  if(NOT DEFINED ARG_SCOPE)
    set(ARG_SCOPE DIRECTORY)
  endif()

  if(NOT DEFINED ARG_FULL_DOCS)
    set(ARG_FULL_DOCS "${ARG_DOCS}")
  endif()

  define_property(
    ${ARG_SCOPE}
    PROPERTY DUNE_${ARG_NAME}
    ${inherited}
    BRIEF_DOCS "${ARG_DOCS}"
    FULL_DOCS "${ARG_FULL_DOCS}"
    )

endfunction()


function(dune_define_project_property)

  dune_parse_function_arguments(
    single NAME DOCS FULL_DOCS
    required NAME DOCS
    )

  if(DEFINED "ARG_FULL_DOCS")
    set(full_docs "FULL DOCS \"${ARG_FULL_DOCS}\"")
  endif()

  dune_define_property(
    NAME ${ARG_NAME}
    SCOPE DIRECTORY
    DOCS ${ARG_DOCS}
    ${full_docs}
    )

endfunction()


function(dune_set_property)

  dune_parse_function_arguments(
    single NAME MODE
    multi SCOPE VALUE
    required SCOPE NAME VALUE
    )

  set_property(
    ${ARG_SCOPE}
    ${ARG_MODE}
    PROPERTY DUNE_${ARG_NAME}
    "${ARG_VALUE}"
    )

endfunction()


function(dune_get_property)

  dune_parse_function_arguments(
    single NAME VAR MODE
    multi SCOPE
    required SCOPE NAME
    )

  if(NOT DEFINED ARG_VAR)
    string(TOLOWER DUNE_${ARG_NAME} ARG_VAR)
  endif()

  get_property(
    value
    ${ARG_SCOPE}
    PROPERTY DUNE_${ARG_NAME}
    ${ARG_MODE}
    )

  set(${ARG_VAR} "${value}" PARENT_SCOPE)

endfunction()


function(dune_set_project_property)

  dune_parse_function_arguments(
    flags MAIN_PROJECT
    single NAME PROJECT MODE
    multi VALUE
    required NAME VALUE
    )

  if(ARG_MAIN_PROJECT)
    set(ARG_PROJECT "${CMAKE_PROJECT_NAME}")
  endif()

  if(NOT DEFINED ARG_PROJECT)
    set(ARG_PROJECT ${PROJECT_NAME})
  endif()

  set_property(
    GLOBAL
    ${ARG_MODE}
    PROPERTY DUNE_${ARG_PROJECT}_${ARG_NAME}
    "${ARG_VALUE}"
    )

endfunction()


function(dune_get_project_property)

  dune_parse_function_arguments(
    flags MAIN_PROJECT
    single NAME PROJECT VAR MODE
    required NAME
    )

  if(ARG_MAIN_PROJECT)
    set(ARG_PROJECT "${CMAKE_PROJECT_NAME}")
  endif()

  if(NOT DEFINED ARG_PROJECT)
    set(ARG_PROJECT ${PROJECT_NAME})
  endif()

  if(NOT DEFINED ARG_VAR)
    string(TOLOWER "DUNE_${ARG_NAME}" ARG_VAR)
  endif()

  get_property(
    value
    GLOBAL
    PROPERTY DUNE_${ARG_PROJECT}_${ARG_NAME}
    ${ARG_MODE}
    )

  set(${ARG_VAR} ${value} PARENT_SCOPE)

endfunction()


dune_define_project_property(NAME PROJECT_NAME DOCS "The name of the current DUNE project")
dune_define_project_property(NAME PROJECT_VERSION DOCS "The version of the current DUNE project")
dune_define_project_property(NAME PROJECT_MAINTAINER DOCS "The maintainer of the current DUNE project")
dune_define_project_property(NAME PROJECT_DESCRIPTION DOCS "The description of the current DUNE project")
dune_define_project_property(NAME PROJECT_REPOSITORY DOCS "The repository URL of the current DUNE project")
dune_define_project_property(NAME PROJECT_DEPENDENCIES DOCS "Other DUNE modules that the current DUNE project depends on")

dune_define_project_property(NAME PROJECT_CMAKE_MODULES DOCS "list of CMake modules")
dune_define_project_property(NAME PROJECT_LIBRARIES DOCS "list of CMake libraries")
dune_define_project_property(NAME PROJECT_EXPORTED_LIBRARIES DOCS "list of exported CMake libraries")
dune_define_project_property(NAME PROJECT_DECLARED_VARIABLES DOCS "list of declared CMake variables")
dune_define_project_property(NAME PROJECT_EXPORTED_VARIABLES DOCS "list of exported CMake variables")
dune_define_project_property(NAME PROJECT_DECLARED_DEFINES DOCS "list of declared CMake defines")
dune_define_project_property(NAME PROJECT_EXPORTED_DEFINES DOCS "list of exported CMake defines")
dune_define_project_property(NAME PROJECT_EXPORTED_IMPORTED_EXECUTABLES DOCS "list of re-exported imported executables")
dune_define_project_property(NAME PROJECT_EXTERNAL_LIBRARIES DOCS "list of CMake external libraries")
dune_define_project_property(NAME PROJECT_FOUND_COMPONENTS DOCS "list of found CMake components")
dune_define_project_property(NAME PROJECT_COMPONENTS_WITH_DESCRIPTION DOCS "list of CMake components")
dune_define_project_property(NAME PROJECT_DEFAULT_TEST_LABELS DOCS "default list of test labels")
dune_define_project_property(NAME PROJECT_MAIN_LIBRARY DOCS "name of the main library")
dune_define_project_property(NAME PROJECT_INSTALL_PROGRAMS DOCS "list of programs installed by the current module")
dune_define_project_property(NAME PROJECT_INSTALLED_EXPORTS DOCS "list of CMake export sets that have already been installed by the current module")
dune_define_project_property(NAME PROJECT_BUILD_DOXYGEN DOCS "flag that controls whether Doxygen documentation will be built")
dune_define_project_property(NAME PROJECT_EXPORT_UNPREFIXED_MACROS DOCS "flag that controls whether preprocessor macros are exported without a prefix")
dune_define_project_property(NAME PROJECT_GROUP DOCS "project group that the current project belongs to")
dune_define_project_property(NAME PROJECT_DEFINE_PREFIX DOCS "Prefix for preprocessor defines")
dune_define_project_property(NAME PROJECT_CONFIG_HEADER DOCS "Path to project config header file")

dune_define_project_property(NAME DOCUMENTED_HEADERS DOCS "list of C++ header files that contain API documentation (mostly used as input to Doxygen)")
dune_define_project_property(NAME DOXYGEN_INPUT_FILES DOCS "list of custom Doxygen input files")
dune_define_project_property(NAME DOXYGEN_INPUT_DIRECTORIES DOCS "list of directories that contain API headers")
dune_define_project_property(NAME DOXYGEN_EXCLUDE_FILES DOCS "list of custom files to be ignored by Doxygen")
dune_define_project_property(NAME DOXYGEN_EXCLUDE_DIRECTORIES DOCS "list of directories to be ignored by Doxygen")
dune_define_project_property(NAME DOXYGEN_EXCLUDE_DIRECTORIES DOCS "list of directories to be ignored by Doxygen")

dune_define_project_property(NAME PROJECT_CMAKE_CONFIG_CODE DOCS "project-specific code in package config.cmake file")
dune_define_project_property(NAME PROJECT_EARLY_CMAKE_CONFIG_CODE DOCS "early project-specific code in package config.cmake file")
dune_define_project_property(NAME PROJECT_UNCONDITIONAL_CMAKE_CONFIG_CODE DOCS "project-specific code at the beginning of the package config.cmake file")


dune_define_property(SCOPE GLOBAL NAME COMMON_TEMPLATE_DIR DOCS "path to the CMake templates directory of dune-common")


dune_define_property(
  SCOPE DIRECTORY
  NAME HEADER_VERSION_PREFIX
  DOCS "prefix path for header files"
  )

dune_define_property(
  SCOPE TARGET
  NAME MODULE_LIBRARIES
  DOCS "list of standard libraries exported by DUNE module in the dependency set as defined by the dune.module file"
  )

dune_define_property(
  SCOPE TARGET
  NAME IS_SUBLIBRARY
  DOCS "flag that specifies whether a library is a sublibrary"
  )
