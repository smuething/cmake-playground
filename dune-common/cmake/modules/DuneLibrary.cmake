include_guard(GLOBAL)

include(DuneParseArguments)
include(DuneUtilities)
include(FeatureSummary)

function(dune_configure_target)

  dune_parse_function_arguments(
    flags
      MAIN_LIBRARY
    single
      TARGET
      CXX_STANDARD
    multi
      COMPILE_OPTIONS
      INCLUDE_DIRECTORIES
      COMPILE_DEFINITIONS
      LIBRARIES
      PRIVATE_COMPILE_OPTIONS
      PRIVATE_INCLUDE_DIRECTORIES
      PRIVATE_COMPILE_DEFINITIONS
      PRIVATE_LIBRARIES
      INCLUDE_SUBDIRECTORIES
      PRIVATE_INCLUDE_SUBDIRECTORIES
      SYSTEM_INCLUDE_DIRECTORIES
    )

  if(NOT ARG_TARGET)
    dune_get_project_property(
      VAR main_library
      PROPERTY MAIN_LIBRARY
      )
    set(ARG_TARGET ${main_library})
  endif()

  get_property(type TARGET ${ARG_TARGET} PROPERTY TYPE)

  if(type STREQUAL EXECUTABLE)
    set(public_specifier PUBLIC)
  elseif(type STREQUAL STATIC_LIBRARY)
    set(public_specifier PUBLIC)

  elseif(type STREQUAL SHARED_LIBRARY)
    set(public_specifier PUBLIC)

  elseif(type STREQUAL INTERFACE_LIBRARY)
    set(public_specifier INTERFACE)

  endif()


  if (ARG_CXX_STANDARD)
    target_compile_features(
      ${ARG_TARGET}
      ${public_specifier} "cxx_std_${ARG_CXX_STANDARD}"
      )
  endif()

  if (ARG_COMPILE_OPTIONS)
    target_compile_options(
      ${ARG_TARGET}
      ${public_specifier} ${ARG_COMPILE_OPTIONS}
      )
  endif()

  if (ARG_COMPILE_DEFINITIONS)
    target_compile_definitions(
      ${ARG_TARGET}
      ${public_specifier} ${ARG_COMPILE_DEFINITIONS}
      )
  endif()

  if (ARG_INCLUDE_DIRECTORIES)
    target_include_directories(
      ${ARG_TARGET}
      ${public_specifier} ${ARG_INCLUDE_DIRECTORIES}
      )
  endif()

  if (ARG_SYSTEM_INCLUDE_DIRECTORIES)
    target_include_directories(
      ${ARG_TARGET}
      SYSTEM
      ${public_specifier} ${ARG_SYSTEM_INCLUDE_DIRECTORIES}
      )
  endif()

  if (ARG_LIBRARIES)
    target_link_libraries(
      ${ARG_TARGET}
      ${public_specifier} ${ARG_LIBRARIES}
      )
  endif()

  if(ARG_PRIVATE_COMPILE_OPTIONS)
    target_compile_options(
      ${ARG_TARGET}
      PRIVATE ${ARG_PRIVATE_COMPILE_OPTIONS}
      )
  endif()

  if(ARG_PRIVATE_COMPILE_DEFINITIONS)
    target_compile_definitions(
      ${ARG_TARGET}
      PRIVATE ${ARG_PRIVATE_COMPILE_DEFINITIONS}
      )
  endif()

  if(ARG_PRIVATE_INCLUDE_DIRECTORIES)
    target_include_directories(
      ${ARG_TARGET}
      PRIVATE ${ARG_PRIVATE_INCLUDE_DIRECTORIES}
      )
  endif()

  if(ARG_PRIVATE_LIBRARIES)
    target_link_libraries(
      ${ARG_TARGET}
      PRIVATE ${ARG_PRIVATE_LIBRARIES}
      )
  endif()


endfunction()

function(dune_add_library)

  dune_parse_function_arguments(
    flags
      INTERFACE
      SKIP_PROJECT_INCLUDE_DIRECTORIES
      SKIP_VERSION_HEADER
      EXTERNAL
      MANUAL_LINKAGE
      SUBLIBRARY
      NO_PROJECT_PREFIX
      MAIN_LIBRARY_COMPAT_MODE
      MAIN_LIBRARY
      NO_EXPORT_NAMESPACE
    single
      NAME
      NAMESPACE
      HEADER_PREFIX
      COMPONENT
      CXX_STANDARD
    multi
      COMPILE_OPTIONS
      INCLUDE_DIRECTORIES
      COMPILE_DEFINITIONS
      LIBRARIES
      LINK_LIBRARIES
      PUBLIC_COMPILE_OPTIONS
      PUBLIC_INCLUDE_DIRECTORIES
      PUBLIC_COMPILE_DEFINITIONS
      PUBLIC_LIBRARIES
      PRIVATE_COMPILE_OPTIONS
      PRIVATE_INCLUDE_DIRECTORIES
      PRIVATE_COMPILE_DEFINITIONS
      PRIVATE_LIBRARIES
      INCLUDE_SUBDIRECTORIES
      SYSTEM_INCLUDE_DIRECTORIES
    )

  if(DEFINED ARG_LINK_LIBRARIES)
    message(SEND_ERROR "You should not use LINK_LIBRARIES, use LIBRARIES instead")
  endif()

  string(COMPARE EQUAL "${ARG_NAME}" "" is_main_library)
  if(ARG_MAIN_LIBRARY)
    set(is_main_library TRUE)
  endif()

  if(ARG_NO_PROJECT_PREFIX)
    set(project_prefix)
  else()
    set(project_prefix "${PROJECT_NAME}-")
  endif()

  if(is_main_library)
    if(ARG_NAME)
      set(NAME "${project_prefix}${ARG_NAME}")
      set(LIBRARY "${project_prefix}${ARG_NAME}")
      set(EXPORT_NAME "${PROJECT_NAME}-${ARG_NAME}")
    else()

      if(ARG_MAIN_LIBRARY_COMPAT_MODE)
        set(main_library_compat_suffix "-cmake-compat")
      endif()

      set(NAME "${PROJECT_NAME}${main_library_compat_suffix}")
      set(LIBRARY "${PROJECT_NAME}${main_library_compat_suffix}")
      set(EXPORT_NAME "${PROJECT_NAME}${main_library_compat_suffix}-main")
    endif()

    if(ARG_SUBLIBRARY)
      message(ERROR "The main library cannot be a sublibrary.")
    endif()

    dune_set_project_property(NAME PROJECT_MAIN_LIBRARY VALUE ${NAME})

    set(main_library ${NAME})

  else()
    set(NAME ${ARG_NAME})

    set(LIBRARY ${project_prefix}${ARG_NAME})

    if(DEFINED ARG_COMPONENT)
      string(REPLACE "::" "-" EXPORT_NAME "${PROJECT_NAME}-${ARG_COMPONENT}")
    else()
      if(ARG_NO_PROJECT_PREFIX)
        set(EXPORT_NAME ${PROJECT_NAME}-${ARG_NAME})
      else()
        set(EXPORT_NAME ${LIBRARY})
      endif()
    endif()

    dune_get_project_property(NAME PROJECT_MAIN_LIBRARY VAR main_library)
  endif()

  if(ARG_SUBLIBRARY)
    set(ARG_SKIP_VERSION_HEADER TRUE)
  endif()

  #if("${ARG_HEADER_PREFIX}" STREQUAL "")
  #  set(ARG_HEADER_PREFIX "${DUNE_HEADER_PREFIX}")
  #else()
  #  # get rid of potential trailing slash
  #  string(REGEX REPLACE "/$" "" ARG_HEADER_PREFIX "${ARG_HEADER_PREFIX}")
  #endif()

  dune_get_project_property(NAME PROJECT_CONFIG_HEADER VAR config_header)
  get_filename_component(config_header_dir "${config_header}" DIRECTORY)
  set(headerguard_helper ${config_header_dir})
  set(config_header_dir "")
  set(LIB_DIR "${PROJECT_BINARY_DIR}/lib")

  if(NOT ARG_INTERFACE)
    file(MAKE_DIRECTORY "${config_header_dir}")
    file(MAKE_DIRECTORY "${LIB_DIR}")

    if(is_main_library)
      dune_mangle_symbol(SYMBOL "${headerguard_helper}_GENERATED_VERSION_HH" UPPERCASE VAR library_version_headerguard)
      dune_mangle_symbol(SYMBOL "${PROJECT_NAME}" LOWERCASE VAR mangled_library_name)
      set(library_version_header "${PROJECT_NAME}-version.hh")
      set(library_version_source "${LIB_DIR}/version.cc")
    else()
      dune_mangle_symbol(SYMBOL "${headerguard_helper}_${NAME}_GENERATED_VERSION_HH" UPPERCASE VAR library_version_headerguard)
      dune_mangle_symbol(SYMBOL "${PROJECT_NAME}_${NAME}" LOWERCASE VAR mangled_library_name)
      set(library_version_header "${PROJECT_NAME}-${NAME}-version.hh")
      set(library_version_source "${LIB_DIR}/${NAME}-version.cc")
    endif()

    dune_get_property(SCOPE GLOBAL NAME COMMON_TEMPLATE_DIR VAR template_dir)

    dune_get_project_property(NAME PROJECT_CONFIG_HEADER)

    configure_file(
      "${template_dir}/library-version.hh.in"
      "${library_version_header}"
      )

    configure_file(
      "${template_dir}/library-version.cc.in"
      "${library_version_source}"
      )

  endif()

  if(ARG_EXTERNAL)

    set(NAME __ext-${NAME})

    add_library(
      ${NAME}
      INTERFACE
      )

    set(public_specifier INTERFACE)

    add_library(ext::${ARG_NAME} ALIAS ${NAME})
    add_library(${namespace}ext::${ARG_NAME} ALIAS ${NAME})

    set_target_properties(${NAME} PROPERTIES EXPORT_NAME "ext::${ARG_NAME}")

    if(DEFINED ARG_NAMESPACE)
      set(namespace "${ARG_NAMESPACE}::")
    else()
      dune_get_project_property(NAME PROJECT_GROUP)
      set(namespace "${dune_project_group}::")
    endif()

    dune_set_project_property(NAME PROJECT_EXTERNAL_LIBRARIES MODE APPEND VALUE "${EXPORT_NAME}\\;${namespace}ext::${ARG_NAME}")

  elseif(ARG_INTERFACE)
    add_library(
      ${NAME}
      INTERFACE
      )

    set(public_specifier INTERFACE)
  else()
    add_library(
      ${NAME}
        "${library_version_source}"
      )

    set_target_properties(
      ${NAME}
      PROPERTIES
        ARCHIVE_OUTPUT_DIRECTORY "${LIB_DIR}"
        LIBRARY_OUTPUT_DIRECTORY "${LIB_DIR}"
        OUTPUT_NAME "${LIBRARY}"
        VERSION "${PROJECT_VERSION}"
        NO_SYSTEM_FROM_IMPORTED ON
      )

    set(public_specifier PUBLIC)
  endif()


  if(ARG_SUBLIBRARY)
    set_target_properties(
      ${NAME}
      PROPERTIES
        DUNE_IS_SUBLIBRARY TRUE
        EXPORT_PROPERTIES "DUNE_IS_SUBLIBRARY;DUNE_MODULE_LIBRARIES"
      )
    #set_property(TARGET ${NAME} PROPERTY EXPORT_PROPERTIES)
    #set_property(TARGET ${NAME} PROPERTY EXPORT_PROPERTIES DUNE_IS_SUBLIRARY)
    #set_property(TARGET ${NAME} PROPERTY DUNE_IS_SUBLIBRARY TRUE)
    #set_property(TARGET ${NAME} APPEND PROPERTY EXPORT_PROPERTIES DUNE_MODULE_LIBRARIES)
  endif()


  if (ARG_CXX_STANDARD)
    target_compile_features(
      ${NAME}
      ${public_specifier} "cxx_std_${ARG_CXX_STANDARD}"
      )
  endif()

  target_compile_options(
    ${NAME}
    ${public_specifier} ${ARG_COMPILE_OPTIONS}
    )

  target_compile_definitions(
    ${NAME}
    ${public_specifier} ${ARG_COMPILE_DEFINITIONS}
    )

  target_include_directories(
    ${NAME}
    ${public_specifier} ${ARG_INCLUDE_DIRECTORIES}
    )

  target_include_directories(
    ${NAME}
    SYSTEM
    ${public_specifier} ${ARG_SYSTEM_INCLUDE_DIRECTORIES}
    )

  target_link_libraries(
    ${NAME}
    ${public_specifier} ${ARG_LIBRARIES} $<$<BOOL:$<TARGET_PROPERTY:DUNE_IS_SUBLIBRARY>>:$<TARGET_PROPERTY:DUNE_MODULE_LIBRARIES>>
    )

  if(ARG_PRIVATE_COMPILE_OPTIONS)
    target_compile_options(
      ${NAME}
      PRIVATE ${ARG_PRIVATE_COMPILE_OPTIONS}
      )
  endif()

  if(ARG_PRIVATE_COMPILE_DEFINITIONS)
    target_compile_definitions(
      ${NAME}
      PRIVATE ${ARG_PRIVATE_COMPILE_DEFINITIONS}
      )
  endif()

  if(ARG_PRIVATE_INCLUDE_DIRECTORIES)
    target_include_directories(
      ${NAME}
      PRIVATE ${ARG_PRIVATE_INCLUDE_DIRECTORIES}
      )
  endif()

  if(ARG_PRIVATE_LIBRARIES)
    target_link_libraries(
      ${NAME}
      PRIVATE ${ARG_PRIVATE_LIBRARIES}
      )
  endif()

  if(NOT (ARG_EXTERNAL OR ARG_INTERFACE))
    target_include_directories(
      ${NAME}
      PRIVATE
        "${PROJECT_SOURCE_DIR}"
        "${PROJECT_BINARY_DIR}"
      )

    # make sure good old HAVE_CONFIG_H works when compiling the library sources
    target_compile_definitions(
      ${NAME}
      PRIVATE HAVE_CONFIG_H
      )

  endif()

  if(NOT ARG_SKIP_PROJECT_INCLUDE_DIRECTORIES)
    target_include_directories(
      ${NAME}
      INTERFACE
        $<INSTALL_INTERFACE:include/${DUNE_GROUP}-${PROJECT_VERSION}> #fixme
        $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
        $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
      )
  endif()

  if(ARG_INCLUDE_SUBDIRECTORIES)

    foreach(subdir ${ARG_INCLUDE_SUBDIRECTORIES})

      target_include_directories(
        ${NAME}
        PRIVATE
          "${PROJECT_SOURCE_DIR}/${subdir}"
          "${PROJECT_BINARY_DIR}/${subdir}"
        )

      target_include_directories(
        ${NAME}
        INTERFACE
          $<INSTALL_INTERFACE:include/${DUNE_GROUP}-${PROJECT_VERSION}/${subdir}> #fixme
          $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/${subdir}>
          $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/${subdir}>
        )

    endforeach()

  endif()

  if(NOT (ARG_INTERFACE OR ARG_SKIP_VERSION_HEADER))

    dune_install_headers(
      HEADERS
        "${PROJECT_BINARY_DIR}/${library_version_header}"
      DESTINATION
        "${ARG_HEADER_PREFIX}"
      ABSOLUTE_INPUT_PATHS
      )
  endif()

  install(
    TARGETS ${NAME}
    EXPORT ${EXPORT_NAME}
    LIBRARY
      DESTINATION "${CMAKE_INSTALL_FULL_LIBDIR}"
      NAMELINK_SKIP
      COMPONENT libraries
# header installation is broken and drops all headers into a single directory
#    PUBLIC_HEADER
#      DESTINATION "${CMAKE_INSTALL_FULL_INCLUDEDIR}/${DUNE_HEADER_VERSION_PREFIX}"
#      COMPONENT devel
    )

  dune_get_project_property(NAME INSTALLED_EXPORTS VAR installed_exports)
  if (NOT ${EXPORT_NAME} IN_LIST installed_exports)
    if(is_main_library)

      install(
        EXPORT ${EXPORT_NAME}
        DESTINATION "lib/cmake/${PROJECT_NAME}-${PROJECT_VERSION}"
        COMPONENT devel
        )

      export(
        EXPORT ${EXPORT_NAME}
        FILE "cmake/${EXPORT_NAME}.cmake"
        )

      set(namespace ${PROJECT_NAME}::)

      set(exported_library ${NAME})

    else()

      if(ARG_NO_EXPORT_NAMESPACE)
        set(namespace "")
      elseif(NOT ARG_EXTERNAL)
        set(namespace "${PROJECT_NAME}::")
      endif()

      install(
        EXPORT ${EXPORT_NAME}
        DESTINATION "lib/cmake/${PROJECT_NAME}-${PROJECT_VERSION}"
        NAMESPACE "${namespace}"
        COMPONENT devel
        )

      export(
        EXPORT ${EXPORT_NAME}
        FILE "cmake/${EXPORT_NAME}.cmake"
        NAMESPACE "${namespace}"
        )

      set(exported_library "${namespace}${NAME}")

    endif()

  endif()

  if(ARG_EXTERNAL)

    # the main library automatically links against all external dependencies
    target_link_libraries(${main_library} PUBLIC ${NAME})

  else()

    add_library(${namespace}${NAME} ALIAS ${NAME})

    if(NOT (ARG_INTERFACE OR ARG_SKIP_PROJECT_INCLUDE_DIRECTORIES))
      target_include_directories(
        ${NAME}
        PRIVATE
          "${PROJECT_BINARY_DIR}/private"
        )
    endif()

    if(NOT ${EXPORT_NAME} IN_LIST installed_exports)

      if(ARG_SUBLIBRARY)
        dune_get_project_property(NAME PROJECT_LIBRARIES VAR exports)
        list(FIND exports "${PROJECT_NAME}-main" main_library_index)
        list(INSERT exports ${main_library_index} ${EXPORT_NAME})
        dune_set_project_property(NAME PROJECT_LIBRARIES VALUE ${exports})
      else()
        dune_set_project_property(NAME PROJECT_LIBRARIES MODE APPEND VALUE ${EXPORT_NAME})
      endif()

    endif()

    if(NOT ARG_MANUAL_LINKAGE)
      dune_set_project_property(NAME PROJECT_EXPORTED_LIBRARIES MODE APPEND VALUE ${exported_library})
    endif()

    if(ARG_SUBLIBRARY)
      if(NOT ARG_MANUAL_LINKAGE)
        get_property(main_library_type TARGET ${main_library} PROPERTY TYPE)
        if(${main_library_type} STREQUAL INTERFACE_LIBRARY)
          set(main_library_link_type INTERFACE)
        else()
          set(main_library_link_type PUBLIC)
        endif()
        target_link_libraries(${main_library} ${main_library_link_type} ${NAME})
      endif()
    else()
      if(NOT (${is_main_library} OR ${ARG_MANUAL_LINKAGE}))
        # all additional libraries automatically link against the main library by default
        target_link_libraries(${NAME} PUBLIC ${main_library})
      endif()
    endif()
  endif()

  dune_set_project_property(NAME PROJECT_INSTALLED_EXPORTS MODE APPEND VALUE ${EXPORT_NAME})

  if(NOT "${ARG_COMPONENT}" STREQUAL "")
    set(HAVE_${ARG_COMPONENT} ON)
  endif()

endfunction()

function(dune_register_component)

  dune_get_project_property(NAME PROJECT_GROUP)

  dune_parse_function_arguments(
    single NAME DESCRIPTION NAMESPACE
    multi FOUND
    required NAME FOUND
    defaults NAMESPACE ${dune_project_group}
    )

  dune_evaluate_boolean(
    RESULT found
    CONDITIONS ARG_FOUND
    )

  set(${PROJECT_NAME}_${ARG_NAME}_FOUND ${found})

  if(found)
    dune_set_project_property(NAME PROJECT_FOUND_COMPONENTS MODE APPEND VALUE ${ARG_NAME})
  endif()

  if(NOT DEFINED ARG_NAMESPACE)
    set(ARG_NAMESPACE ${PROJECT_NAME})
  endif()

  dune_set_project_property(
    NAME PROJECT_COMPONENTS_WITH_DESCRIPTION
    MODE APPEND
    VALUE "${ARG_NAMESPACE}::${ARG_NAME}##${found}##${ARG_DESCRIPTION}"
    )

  add_feature_info(${ARG_NAMESPACE}::${ARG_NAME} ${found} "${ARG_DESCRIPTION}")

endfunction()


function(dune_add_dependency)

  dune_get_project_property(NAME PROJECT_GROUP)

  dune_parse_function_arguments(
    single NAME DESCRIPTION NAMESPACE
    multi COMPILE_DEFINITIONS COMPILE_OPTIONS LIBRARIES INCLUDE_DIRECTORIES FOUND TARGETS
    required NAME
    defaults NAMESPACE ${dune_project_group}
    )

  if(DEFINED ARG_TARGETS)

    if(DEFINED ARG_FOUND)
      dune_evaluate_boolean(
        RESULT found
        CONDITIONS ${ARG_FOUND}
        )
    else()
      set(found TRUE)
    endif()

    if(found)
      foreach(target IN LISTS ARG_TARGETS)
        if(NOT TARGET ${target})
          set(found FALSE)
          break()
        endif() 
      endforeach()
    endif()

  else()

    if(NOT DEFINED ARG_FOUND)
      message(FATAL_ERROR "You must specify a list of tests for the presence of the dependency in FOUND")
    endif()

    dune_evaluate_boolean(
      RESULT found
      CONDITIONS ${ARG_FOUND}
      )

  endif()

  cmake_print_variables(ARG_NAME found)

  dune_register_component(
    NAME ${ARG_NAME}
    FOUND found
    NAMESPACE ${ARG_NAMESPACE}::ext
    DESCRIPTION "${ARG_DESCRIPTION}"
    )

  if(NOT found)
    return()
  endif()

  dune_add_library(
    NAME ${ARG_NAME}
    INTERFACE
    EXTERNAL
    NAMESPACE ${ARG_NAMESPACE}
    SKIP_PROJECT_INCLUDE_DIRECTORIES
    COMPONENT ext::${ARG_NAME}
    COMPILE_DEFINITIONS ${ARG_COMPILE_DEFINITIONS}
    COMPILE_OPTIONS ${ARG_COMPILE_OPTIONS}
    LIBRARIES ${ARG_LIBRARIES}
    )

  if(ARG_TARGETS)

    foreach(target IN LISTS ARG_TARGETS)

      get_target_property(compile_definitions ${target} INTERFACE_COMPILE_DEFINITIONS)
      get_target_property(compile_options ${target} INTERFACE_COMPILE_OPTIONS)
      get_target_property(compile_features ${target} INTERFACE_COMPILE_FEATURES)
      get_target_property(include_directories ${target} INTERFACE_INCLUDE_DIRECTORIES)
      get_target_property(link_directories ${target} INTERFACE_LINK_DIRECTORIES)
      get_target_property(link_libraries ${target} INTERFACE_LINK_LIBRARIES)
      get_target_property(link_options ${target} INTERFACE_LINK_OPTIONS)

      if(compile_definitions)
        target_compile_definitions(__ext-${ARG_NAME} INTERFACE ${compile_definitions})
      endif()

      if(compile_options)
        target_compile_options(__ext-${ARG_NAME} INTERFACE ${compile_options})
      endif()

      if(compile_features)
        target_compile_features(__ext-${ARG_NAME} INTERFACE ${compile_features})
      endif()

      if(include_directories)
        target_include_directories(__ext-${ARG_NAME} INTERFACE ${include_directories})
      endif()

      if(link_directories)
        target_link_directories(__ext-${ARG_NAME} INTERFACE ${link_directories})
      endif()

      if(link_libraries)
        target_link_libraries(__ext-${ARG_NAME} INTERFACE ${link_libraries})
      endif()

      if(link_options)
        target_link_options(__ext-${ARG_NAME} INTERFACE ${link_options})
      endif()

    endforeach()

  endif()

  get_target_property(include_directories __ext-${ARG_NAME} INTERFACE_INCLUDE_DIRECTORIES)
  set_target_properties(__ext-${ARG_NAME} PROPERTIES INTERFACE_SYSTEM_INCLUDE_DIRECTORIES "${include_directrories}")

endfunction()


function(dune_library_add_sources)

  dune_parse_function_arguments(
    single LIBRARY
    multi SOURCES CMAKE_GUARD
    )

  if(NOT DEFINED ARG_LIBRARY)
    dune_get_project_property(NAME PROJECT_MAIN_LIBRARY VAR ARG_LIBRARY)
  endif()

  dune_evaluate_boolean(
    RESULT
      guard
    CONDITIONS
      ${ARG_CMAKE_GUARD}
    )

  if(guard)
    foreach(source ${ARG_SOURCES})
      target_sources(
        ${ARG_LIBRARY}
        PRIVATE
          "${CMAKE_CURRENT_SOURCE_DIR}/${source}"
        )
    endforeach()
  endif()

endfunction()


function(dune_library_add_headers)

  dune_parse_function_arguments(
    single
      LIBRARY
      DESTINATION
    multi
      HEADERS
    required
      HEADERS
    )

  if(NOT DEFINED ARG_LIBRARY)
    dune_get_project_property(NAME PROJECT_MAIN_LIBRARY VAR main_library)
    set(ARG_LIBRARY ${main_library})
  endif()

  file(RELATIVE_PATH basedir "${PROJECT_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}/")

  foreach(header ${ARG_HEADERS})

    set_property(
      TARGET ${ARG_LIBRARY}
      APPEND
      PROPERTY
        PUBLIC_HEADERS
        "${basedir}${header}"
      )

  endforeach()

  # unfortunately we can't install the PUBLIC_HEADERS because CMake stupidly insists
  # on dropping them all in a single directory, so we manually install them as files

  dune_install_headers(HEADERS ${ARG_HEADERS} DESTINATION ${DESTINATION})

endfunction()


function(dune_export_nonbuilt_executable)

  cmake_parse_arguments(
    "ARG"
    "EXTERNAL"
    "NAME;LOCATION;FOUND"
    ""
    "${ARGN}"
    )

  if(DEFINED ARG_FOUND)
    dune_evaluate_boolean(
      RESULT found
      CONDITIONS ${ARG_FOUND}
      )
  else()
    set(found TRUE)
  endif()

  if(found)

    dune_set_project_property(PROPERTY CMAKE_EXPORTED_NONBUILT_EXECUTABLES APPEND VALUE "")

    add_executable("${PROJECT_NAME}::${ARG_NAME}" IMPORTED GLOBAL)
    set_target_properties(
      "${PROJECT_NAME}::${ARG_NAME}"
      PROPERTIES
        IMPORTED_LOCATION "${ARG_EXECUTABLE}"
      )

    if(ARG_EXPORT)

      if(NOT ARG_COMPONENT)
        set(ARG_COMPONENT devel)
      endif()

      install(
        TARGETS
          "${PROJECT_NAME}::${ARG_NAME}"
        EXPORT
          ${ARG_EXPORT}
        RUNTIME
        COMPONENT
          ${ARG_COMPONENT}
        )

    endif()

  endif()

endfunction()


function(dune_add_imported_executable)

  cmake_parse_arguments(
    "ARG"
    "EXPORT"
    "NAME;EXECUTABLE;DESCRIPTION;NAMESPACE"
    "CMAKE_GUARD"
    "${ARGN}"
    )

  dune_evaluate_boolean(
    RESULT found
    CONDITIONS ${ARG_CMAKE_GUARD}
    )

  # the namespace for imported executables defaults to the module group (normally dune)
  if(NOT DEFINED ARG_NAMESPACE)
    dune_get_project_property(PROPERTY MODULE_GROUP VAR ARG_NAMESPACE)
  endif()

  set(target "${ARG_NAMESPACE}::${ARG_NAME}")

  if(found)
    add_executable(${target} IMPORTED GLOBAL)
    set_target_properties(
      ${target}
      PROPERTIES
        IMPORTED_LOCATION "${ARG_EXECUTABLE}"
      )

    if(ARG_EXPORT)

      string(REGEX REPLACE ";" "\\\\;" escaped_executable "${ARG_EXECUTABLE}")

      dune_set_project_property(
        PROPERTY CMAKE_EXPORTED_IMPORTED_EXECUTABLES
        APPEND VALUE
          "${target}##${escaped_executable}"
        )

    endif()

  endif()

  if(DEFINED ARG_DESCRIPTION)

    string(REGEX REPLACE ";" "\\\\;" escaped_description "${ARG_DESCRIPTION}")

    dune_set_project_property(PROPERTY CMAKE_COMPONENTS_WITH_DESCRIPTION APPEND VALUE "${target}##${found}##${escaped_description}")

    add_feature_info("${ARG_NAMESPACE}::${ARG_NAME}" ${found} "${ARG_DESCRIPTION}")

  endif()

endfunction()
