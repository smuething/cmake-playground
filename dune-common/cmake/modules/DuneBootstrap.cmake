# only include this file once!
include_guard(GLOBAL)

macro(dune_cmake_version_setup)

  cmake_parse_arguments(
    _ARG
    ""
    "MIN_VERSION;MAX_VERSION"
    ""
    "${ARGN}"
    )

  if(${CMAKE_VERSION} VERSION_GREATER_EQUAL 3.12)

    if(DEFINED _ARG_MAX_VERSION)
      set(_dune_version_range "${_ARG_MIN_VERSION}...${_ARG_MAX_VERSION}")
    else()
      set(_dune_version_range ${_ARG_MIN_VERSION})
    endif()

    cmake_minimum_required(VERSION ${_dune_version_range} FATAL_ERROR)

  else()

    cmake_minimum_required(VERSION${_ARG_MIN_VERSION} FATAL_ERROR)

    if(DEFINED _ARG_MAX_VERSION)

      if(${CMAKE_VERSION} VERSION_GREATER_EQUAL ${_ARG_MAX_VERSION})
        cmake_policy(VERSION ${_ARG_MAX_VERSION})
      else()
        cmake_policy(VERSION ${CMAKE_VERSION})
      endif()

    endif()

  endif()

endmacro()

set(DUNE_MIN_CMAKE_VERSION 3.11.0 CACHE STRING "The minimum version of CMake required by the DUNE build system")
mark_as_advanced(DUNE_MIN_CMAKE_VERSION)

set(DUNE_MAX_CMAKE_VERSION 3.15.1 CACHE STRING "The maximum version of CMake supported by the DUNE build system. Policies from newer CMake versions will be disabled.")
mark_as_advanced(DUNE_MAX_CMAKE_VERSION)

option(DUNE_DISABLE_AUTOMATIC_CMAKE_VERSION_SETUP "Disable the automatic CMake version setup that is normally performed when including DuneBootstrap")
mark_as_advanced(DUNE_DISABLE_AUTOMATIC_CMAKE_VERSION_SETUP)

option(DUNE_DISABLE_WRAPPED_PROJECT_COMMAND "Do not replace the built-in project() command with a DUNE-specific wrapper")
mark_as_advanced(DUNE_DISABLE_WRAPPED_PROJECT_COMMAND)

if(NOT DUNE_DISABLE_AUTOMATIC_CMAKE_VERSION_SETUP)
  message(STATUS "DUNE: Setting supported CMake version range to (${DUNE_MIN_CMAKE_VERSION} ... ${DUNE_MAX_CMAKE_VERSION})")
  dune_cmake_version_setup(MIN_VERSION ${DUNE_MIN_CMAKE_VERSION} MAX_VERSION ${DUNE_MAX_CMAKE_VERSION})
endif()

include(DuneProperties)

get_filename_component(template_dir "${CMAKE_CURRENT_LIST_DIR}/../templates/" ABSOLUTE)
dune_set_property(SCOPE GLOBAL NAME COMMON_TEMPLATE_DIR VALUE "${template_dir}")

if (NOT DUNE_DISABLE_WRAPPED_PROJECT_COMMAND)

  message(STATUS "DUNE: Installing wrapper around built-in project() command")
  include(DuneParseModuleFile)

  macro(project)

    # Is this project a Dune modules?
    if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/dune.module")

      if(NOT DUNE_DISABLE_AUTOMATIC_CMAKE_VERSION_SETUP)
        dune_cmake_version_setup(MIN_VERSION ${DUNE_MIN_CMAKE_VERSION} MAX_VERSION ${DUNE_MAX_CMAKE_VERSION})
      endif()

      cmake_parse_arguments(
        _ARG
        ""
        "NAME"
        "LANGUAGES"
        "${ARGN}"
        )

      # We always enable C and C++
      if(NOT DEFINED _ARG_LANGUAGES)
        set(_ARG_LANGUAGES C CXX)
      else()
        list(PREPEND _ARG_LANGUAGES C CXX)
        list(REMOVE_DUPLICATES _ARG_LANGUAGES)
      endif()


      dune_parse_module_file(NAME_VAR _name)

      # re-run CMake if the dune.module file is changed
      set_property(DIRECTORY APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS dune.module)

      dune_get_project_property(PROJECT ${_name} NAME PROJECT_VERSION VAR _version)
      dune_get_project_property(PROJECT ${_name} NAME PROJECT_DESCRIPTION VAR _description)
      dune_get_project_property(PROJECT ${_name} NAME PROJECT_REPOSITORY_URL VAR _url)

      _project(
        ${_name}
        DESCRIPTION ${_description}
        HOMEPAGE_URL ${_url}
        VERSION 1.2.3
        LANGUAGES
          ${_ARG_LANGUAGES}
        )

      # We have to manually set the version variables because CMake doesn't support
      # non-numeric version components

      set(PROJECT_VERSION ${_version})
      set(${_name}_VERSION ${_version})
      if(${CMAKE_PROJECT_NAME} STREQUAL ${PROJECT_NAME})
        set(CMAKE_PROJECT_VERSION ${_version})
      endif()

      string(REPLACE "." ";" _version "${_version}")
      set(_components MAJOR MINOR PATCH TWEAK)

      foreach(_comp IN LISTS _components)
        set(PROJECT_VERSION_${_comp} 0)
        if(${CMAKE_PROJECT_NAME} STREQUAL ${PROJECT_NAME})
          set(CMAKE_PROJECT_VERSION_${_comp} 0)
        endif()
      endforeach()

      dune_list_indices(LIST _version VAR _version_indices)

      foreach(_index IN LISTS _version_indices)
        list(GET _components ${_index}  _comp)
        list(GET _version ${_index}  _value)

        string(REGEX REPLACE "(0|[1-9][0-9]*).*" "\\1" _value "${_value}")

        if("${_value}" STREQUAL "")
          set(_value 0)
        endif()

        set(PROJECT_VERSION_${_comp} ${_value})
        set(${_name}_VERSION_${_comp} ${_value})
        if(${CMAKE_PROJECT_NAME} STREQUAL ${PROJECT_NAME})
          set(CMAKE_PROJECT_VERSION_${_comp} ${_value})
        endif()
      endforeach()

      include(DuneProject)

      dune_find_dependencies()

      # forward quoted arguments to preserve possible empty arguments
      dune_project("${ARGV}")

    else()

      cmake_parse_arguments(
        _ARG
        "DUNE_MODULE_COLLECTION"
        "NAME;VERSION"
        ""
        "${ARGN}"
        )

      if (_ARG_DUNE_MODULE_COLLECTION)

        # only set up DUNE module collections in the top directory of a source tree
        if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_SOURCE_DIR}")

          if("${_ARG_NAME}" STREQUAL "")
            message(FATAL_ERROR "DUNE: You must provide a NAME <name> argument when creating a DUNE module collection")
          endif()

          if(NOT "${_ARG_VERSION}" STREQUAL "")
            set(_ARG_VERSION "VERSION ${_ARG_VERSION}")
          endif()

          message(STATUS "DUNE: Setting up module collection ${_ARG_NAME}")
          _project(
            ${_ARG_NAME}
            ${_ARG_VERSION}
            )

          include(DuneModuleCollection)
          dune_module_collection("${ARGV}")

        else()

          message(FATAL_ERROR "DUNE: You can only create a module collection at the top of your source directory hierarchy.")

        endif()

      else()

        # Be nice to possible non-DUNE subprojects and forward the call unmodified
        message(STATUS "DUNE: Project ${ARGV0} contains no dune.module file, setting up regular CMake project")
        _project(${ARGV})

      endif()

    endif()

  endmacro()

endif()
