# only include this file once!
include_guard(GLOBAL)

set_property(GLOBAL PROPERTY DUNE_LIST_INDICES "\
0;1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;\
21;22;23;24;25;26;27;28;29;30;31;32;33;34;35;36;37;38;39;40;\
41;42;43;44;45;46;47;48;49;50;51;52;53;54;55;56;57;58;59;60;\
61;62;63;64;65;66;67;68;69;70;71;72;73;74;75;76;77;78;79;80;\
81;82;83;84;85;86;87;88;89;90;91;92;93;94;95;96;97;98;99;100;\
101;102;103;104;105;106;107;108;109;110;111;112;113;114;115;116;117;118;119;120;\
121;122;123;124;125;126;127;128;129;130;131;132;133;134;135;136;137;138;139;140;\
141;142;143;144;145;146;147;148;149;150;151;152;153;154;155;156;157;158;159;160;\
161;162;163;164;165;166;167;168;169;170;171;172;173;174;175;176;177;178;179;180;\
181;182;183;184;185;186;187;188;189;190;191;192;193;194;195;196;197;198;199"
  )

function(dune_list_indices)

  cmake_parse_arguments(
    PARSE_ARGV 0
    ARG
    ""
    "VAR;LIST;LENGTH"
    ""
    )

  if(DEFINED ARG_LIST)
    list(LENGTH ${ARG_LIST} ARG_LENGTH)
  endif()

  if(${ARG_LENGTH} GREATER 200)
    message(FATAL_ERROR "Cannot generate indices for list with more than 200 entries (your list has ${length} entries)")
  endif()

  get_property(all_indices GLOBAL PROPERTY DUNE_LIST_INDICES)
  list(SUBLIST all_indices 0 ${ARG_LENGTH} indices)

  set(${ARG_VAR} ${indices} PARENT_SCOPE)

endfunction()


function(dune_escape_list)

  dune_parse_function_arguments(
    single LIST VAR
    multi VALUES
    )

  if(NOT ARG_VAR)
    if(ARG_LIST)
      set(ARG_VAR ${ARG_LIST})
    else()
      message(FATAL_ERROR "You must specify the target variable VAR when not passing a LIST argument")
    endif()
  endif()

  if(ARG_LIST)
    if(ARG_VALUES)
      message(FATAL_ERROR "You must specify either a LIST or a list of VALUES, not both")
    endif()
    string(REPLACE ";" "\\;" output "${${ARG_LIST}}")
  elseif(ARG_VALUES)
    string(REPLACE ";" "\\;" output "${ARG_VALUES}")
  else()
    message(FATAL_ERROR "You must specify either a LIST or a list of VALUES")
  endif()

  set(${ARG_VAR} "${output}" PARENT_SCOPE)

endfunction()


macro(dune_parse_function_arguments)

  set(_dpa_A A)
  unset(_dpa_have_error)
  unset(_dpa_UNPARSED_ARGUMENTS)
  unset(_dpa_unparsed_ok)
  unset(_dpa_unparsed_warn)
  unset(_dpa_trace)
  unset(_dpa_unpack_forwarded_arguments)
  unset(_dpa_flags)
  unset(_dpa_single)
  unset(_dpa_multi)
  unset(_dpa_required)
  unset(_dpa_defaults)
  cmake_parse_arguments(
    _dpa
    "unparsed_ok;unparsed_warn;trace;unpack_forwarded_arguments"
    ""
    "flags;single;multi;required;defaults"
    "${ARGN}"
    )

  if(DEFINED _dpa_UNPARSED_ARGUMENTS)
    message(FATAL_ERROR "Unknown arguments in call to dune_parse_function_arguments(): ${_dpa_UNPARSED_ARGUMENTS}")
  endif()

  # We need to do some clever trickery to get at ARGC and ARGV#. Neither of those can appear as a literal,
  # because that would immediately get replaced by the CMake parser with the information found at the call
  # site of the macro, so we have to use string substitution to build those names at runtime
  if(_dpa_unpack_forwarded_arguments)

    set(_dpa_i 0)
    set(_dpa_forwarded "${ARGV${_dpa_i}}")

    list(LENGTH _dpa_forwarded _dpa_argc)

    set(${_dpa_A}RGC ${_dpa_argc})

    dune_list_indices(LENGTH ${_dpa_argc} VAR _dpa_argv_indices)

    foreach(_dpa_i IN LISTS _dpa_argv_indices)
      list(GET _dpa_forwarded ${_dpa_i} ARGV${_dpa_i})
    endforeach()

  else()

    set(_dpa_argc "${${_dpa_A}RGC}")

    dune_list_indices(LENGTH ${_dpa_argc} VAR _dpa_argv_indices)

  endif()

  if(_dpa_trace)
    set(_dpa_msg "dune_parse_function_arguments() called for (")
    foreach(_dpa_i IN LISTS _dpa_argv_indices)
      string(APPEND _dpa_msg " \"${ARGV${_dpa_i}}\"")
    endforeach()
    string(APPEND _dpa_msg ")")
    message(STATUS "${_dpa_msg}")
  endif()

  foreach(_dpa_i IN LISTS _dpa_argv_indices)
    if("${ARGV${_dpa_i}}" STREQUAL "")
      set(ARGV${_dpa_i} "__dpa_mark_empty")
    endif()
  endforeach()

  set(_dpa_type args)
  foreach(_dpa_i IN LISTS _dpa_defaults)
    list(APPEND _dpa_${_dpa_type} "${_dpa_i}")
    if ("${_dpa_type}" STREQUAL "args")
      set(_dpa_type values)
    else()
      set(_dpa_type args)
    endif()
  endforeach()

  if ("${_dpa_type}" STREQUAL "values")
    list(GET _dpa_defaults -1 _dpa_var)
    message(FATAL_ERROR "Missing default value for ${_dpa_var} in call to dune_parse_function_arguments()!")
  else()
    dune_list_indices(LIST _dpa_args VAR _dpa_defaults_indices)
  endif()

  # Remove any arguments possibly inherited from a parent scope
  foreach(_dpa_arg IN LISTS _dpa_flags _dpa_single _dpa_multi)
    unset(ARG_${_dpa_arg})
  endforeach()

  cmake_parse_arguments(
    PARSE_ARGV 0
    ARG
    "${_dpa_flags}"
    "${_dpa_single}"
    "${_dpa_multi}"
    )

  set(_dpa_have_error FALSE)

  foreach(_dpa_arg IN LISTS _dpa_required)

    if(NOT DEFINED "ARG_${_dpa_arg}")
      message(SEND_ERROR "Missing argument detected in function call: ${_dpa_arg}")
      set(_dpa_have_error TRUE)
    endif()

  endforeach()

  foreach(_dpa_arg IN LISTS _dpa_single _dpa_multi)
    if(DEFINED ARG_${_dpa_arg})
      string(REPLACE "__dpa_mark_empty" "" ARG_${_dpa_arg} "${ARG_${_dpa_arg}}")
    endif()
  endforeach()

  foreach(_dpa_i IN LISTS _dpa_defaults_indices)

    list(GET _dpa_args ${_dpa_i} _dpa_arg)

    if(NOT DEFINED "ARG_${_dpa_arg}")
      list(GET _dpa_values ${_dpa_i} ARG_${_dpa_arg})
    endif()

  endforeach()

  if(NOT _dpa_unparsed_ok AND DEFINED "ARG_UNPARSED_ARGUMENTS")
    if(_dpa_unparsed_warn)
      set(_dpa_msg_type WARNING)
    else()
      set(_dpa_msg_type SEND_ERROR)
      set(_dpa_have_error TRUE)
    endif()
    message(${_dpa_msg_type} "Unknown arguments detected in function call: ${ARG_UNPARSED_ARGUMENTS}")
  endif()

  if(_dpa_trace)

    set(_dpa_msg "flags:")
    foreach(_dpa_arg IN LISTS _dpa_flags)
      string(APPEND _dpa_msg " ${_dpa_arg}='${ARG_${_dpa_arg}}'")
    endforeach()

    string(APPEND _dpa_msg " single:")
    foreach(_dpa_arg IN LISTS _dpa_single)
      if(DEFINED ARG_${_dpa_arg})
        string(APPEND _dpa_msg " ${_dpa_arg}='${ARG_${_dpa_arg}}'")
      endif()
    endforeach()

    string(APPEND _dpa_msg " multi:")
    foreach(_dpa_arg IN LISTS _dpa_multi)
      if(DEFINED ARG_${_dpa_arg})
        string(APPEND _dpa_msg " ${_dpa_arg}='${ARG_${_dpa_arg}}'")
      endif()
    endforeach()

    message(STATUS "${_dpa_msg}")
  endif()

  if(_dpa_have_error)
    message(FATAL_ERROR "Invalid signature in function call, aborting")
  endif()

endmacro()
