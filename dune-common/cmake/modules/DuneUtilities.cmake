include_guard(GLOBAL)

include(DuneParseArguments)

function(dune_mangle_symbol)

  dune_parse_function_arguments(
    flags UPPERCASE LOWERCASE NONFATAL_FAILURE
    single SYMBOL VAR REPLACE REPLACEMENT FIRST PROHIBITED
    defaults
      FIRST "[a-zA-Z]"
      REPLACE "[^a-zA-Z0-9_]"
      REPLACEMENT "_"
    )

  set(message "")
  if(NOT "${ARG_SYMBOL}" MATCHES "^${ARG_FIRST}")
    set(message "Cannot mangle symbol \"${ARG_SYMBOL}\", invalid initial character. Valid characters: ${ARG_FIRST}")
  endif()

  if(ARG_PROHIBITED AND "${ARG_SYMBOL}" MATCHES "${ARG_PROHIBITED}")
    set(message "Cannot mangle symbol \"${ARG_SYMBOL}\", found prohibited character. Prohibited characters : ${ARG_PROHIBITED}")
  endif()

  if(message)
    if(ARG_NONFATAL_FAILURE)
      set(message_type VERBOSE)
      set(${ARG_VAR} NOTFOUND PARENT_SCOPE)
      set(${ARG_VAR}_OK FALSE PARENT_SCOPE)
    else()
      set(message_type FATAL_ERROR)
    endif()
    message(${message_type} "${message}")
  endif()

  string(REGEX REPLACE "${ARG_REPLACE}" "${ARG_REPLACEMENT}" var "${ARG_SYMBOL}")

  if(ARG_UPPERCASE)
    string(TOUPPER "${var}" var)
  endif()

  if(ARG_LOWERCASE)
    string(TOLOWER "${var}" var)
  endif()

  set(${ARG_VAR} "${var}" PARENT_SCOPE)

endfunction()
