include_guard(GLOBAL)

include(DuneParseArguments)
include(DuneParseModuleFile)
include(DuneFinalize)
include(DuneProperties)

function(dune_module_collection)

  dune_parse_function_arguments(
    unpack_forwarded_arguments
    flags DUNE_MODULE_COLLECTION
    single NAME VERSION
    )

  dune_register_finalizer(CODE "_dune_finalize_module_collection()")

endfunction()


function(dune_add_modules)

  dune_parse_function_arguments(
    multi DIRECTORIES
    )

  foreach(dir IN LISTS ARG_DIRECTORIES)

    dune_parse_module_file(PATH "${CMAKE_CURRENT_SOURCE_DIR}/${dir}" NAME_VAR name)
    dune_set_property(SCOPE GLOBAL NAME MODULE_COLLECTION_MODULES MODE APPEND VALUE ${name})

  endforeach()

endfunction()

macro(_expand_dependencies module)

  if(NOT ${module}_expanded)
    if(${module} IN_LIST dep_stack)
      list(JOIN dep_stack " " stack)
      message(FATAL_ERROR "Found circular dependency between modules:\n ${stack}")
    endif()
    list(APPEND dep_stack ${module})
    foreach(dep IN LISTS ${module}_deps)
      _expand_dependencies(${dep})
      list(APPEND ${module}_expanded_deps ${${dep}_expanded_deps} ${dep})
    endforeach()
    list(REMOVE_DUPLICATES ${module}_expanded_deps)
    set(${module}_expanded TRUE)
    list(REMOVE_AT dep_stack -1)
  endif()

endmacro()

function(_dune_finalize_module_collection)

  dune_get_property(SCOPE GLOBAL NAME MODULE_COLLECTION_MODULES VAR modules)

  foreach(module IN LISTS modules)

    dune_get_project_property(PROJECT ${module} NAME PROJECT_DEPENDENCIES VAR deps)

    foreach(dep IN LISTS deps)
      list(GET dep 0 name)
      if("${name}" IN_LIST modules)
        list(APPEND ${module}_deps ${name})
      endif()
    endforeach()

  endforeach()

  foreach(module IN LISTS modules)
    _expand_dependencies(${module})
  endforeach()

  while(NOT "${modules}" STREQUAL "")
    foreach(module IN LISTS modules)

      if("${${module}_expanded_deps}" STREQUAL "")
        list(APPEND build_order ${module})
        list(FILTER modules EXCLUDE REGEX "^${module}$")
        foreach(mod IN LISTS modules)
          list(FILTER ${mod}_expanded_deps EXCLUDE REGEX "^${module}$")
        endforeach()
        break()
      endif()

    endforeach()
  endwhile()

  list(JOIN build_order " " build_order_msg)
  message(STATUS "DUNE: Build order: ${build_order_msg}")

  foreach(module IN LISTS build_order)
    file(
      RELATIVE_PATH
      subdir
      ${CMAKE_CURRENT_SOURCE_DIR}
      $CACHE{DUNE_${module}_PROJECT_DIR}
      )
    add_subdirectory(${subdir})
  endforeach()

  foreach(module IN LISTS build_order)
    find_package(${module} QUIET REQUIRED)
  endforeach()

  feature_summary(WHAT ALL)

endfunction()
