# only include this file once!
include_guard(GLOBAL)

include(DuneParseArguments)
include(DuneProperties)

function(dune_parse_module_file_entry)

  dune_parse_function_arguments(
    flags OPTIONAL
    single PATH KEY RESULT DEFAULT
    required KEY
    defaults PATH "${CMAKE_CURRENT_SOURCE_DIR}"
    )

  set(modfile "${ARG_PATH}/dune.module")

  if(NOT DEFINED "ARG_RESULT")
    string(TOLOWER "${ARG_KEY}" ARG_RESULT)
  endif()

  file(STRINGS "${modfile}" lines REGEX "^${ARG_KEY}:")

  list(LENGTH lines count)
  if (count EQUAL 0)

    if (DEFINED "ARG_DEFAULT")
      set(${ARG_RESULT} "${ARG_DEFAULT}" PARENT_SCOPE)
      return()
    endif()

    if (ARG_OPTIONAL)
      set(${ARG_RESULT} ${ARG_KEY}-NOTFOUND PARENT_SCOPE)
      return()
    endif()

    message(FATAL_ERROR "Missing key ${ARG_KEY} in file ${modfile}")
  endif()

  if (count GREATER 1)
    message(FATAL_ERROR "Duplicate key ${ARG_KEY} in file ${modfile}")
  endif()

  string(REGEX REPLACE "^${ARG_KEY}:(.*)$" "\\1" line "${lines}")
  string(STRIP "${line}" line)
  set(${ARG_RESULT} "${line}" PARENT_SCOPE)

endfunction()



set(DUNE_MODULE_NAME_PATTERN "[a-zA-Z]+(-[a-zA-Z]+)*")
set(DUNE_BASIC_VERSION_PATTERN "[1-9][0-9]*(\.[0-9]+)*([-.][a-zA-Z0-9][_a-zA-Z0-9]*)?")
set(DUNE_COMPONENT_PATTERN "[a-z0-9]+(-[a-z0-9]+)*")
set(DUNE_COMPONENTS_PATTERN "\\[( *${DUNE_COMPONENT_PATTERN}\\??)?( *, *${DUNE_COMPONENT_PATTERN}\\??)* *\\]")
set(DUNE_MODULE_VERSION_PATTERN "\\( *((<|>|<=|>=|=)? *${DUNE_BASIC_VERSION_PATTERN}) *\\)")
set(DUNE_DEPENDENCY_PATTERN "${DUNE_MODULE_NAME_PATTERN} *(\\[[-a-z0-9?, ]*\\])? *(\\([^)]*\\))?")

function(dune_parse_module_spec_list)

  dune_parse_function_arguments(
    single MODULES PREFIX
    required MODULES
    defaults
      PREFIX SPEC
    )

  # strip whitespace from list
  string(STRIP "${ARG_MODULES}" module_specs)

  # validate syntax
  if(NOT "${module_specs}" MATCHES "^(${DUNE_DEPENDENCY_PATTERN} *)*$")
    set(${ARG_PREFIX}_OK FALSE PARENT_SCOPE)
    return()
  endif()

  # chomp into individual entries
  string(REGEX MATCHALL "${DUNE_DEPENDENCY_PATTERN}" module_specs "${module_specs}")

  # parse individual entries
  foreach(spec IN LISTS module_specs)

    if("${spec}" MATCHES "^(${DUNE_MODULE_NAME_PATTERN}) *(\\[[-a-z0-9?, ]*\\])? *(${DUNE_MODULE_VERSION_PATTERN})?$")
      list(APPEND names "${CMAKE_MATCH_1}")

      if("${spec}" MATCHES "${DUNE_COMPONENTS_PATTERN}")
        string(REGEX MATCHALL "${DUNE_COMPONENT_PATTERN}\\??" module_components "${CMAKE_MATCH_0}")

        set(module_components_list "")
        foreach(component IN LISTS module_components)
          if(${component} MATCHES "(.*)\\?")
            set(component ${CMAKE_MATCH_1})
            set(required FALSE)
          else()
            set(required TRUE)
          endif()
          list(APPEND module_components_list "${component}\\;${required}")
        endforeach()

        string(REPLACE ";" "\\;" module_components_list "${module_components_list}")
        list(APPEND components "${module_components_list}")

      elseif("${spec}" MATCHES "\\[[-a-z0-9?, ]*\\]")
        set(${ARG_PREFIX}_OK FALSE PARENT_SCOPE)
        return()
      else()
        list(APPEND components NOTFOUND)
      endif()

      if("${spec}" MATCHES "${DUNE_MODULE_VERSION_PATTERN}")
        list(APPEND versions "${CMAKE_MATCH_1}")
      else()
        list(APPEND versions NOTFOUND)
      endif()
    endif()

  endforeach()

  # export results
  set(${ARG_PREFIX}_OK TRUE PARENT_SCOPE)
  set(${ARG_PREFIX}_NAMES "${names}" PARENT_SCOPE)
  set(${ARG_PREFIX}_VERSIONS "${versions}" PARENT_SCOPE)
  set(${ARG_PREFIX}_COMPONENTS "${components}" PARENT_SCOPE)

endfunction()


function(dune_check_module_name)

  dune_parse_function_arguments(
    flags STRICT
    single NAME RESULT
    required NAME
    defaults
      RESULT NAME_OK
    )

  if("${ARG_NAME}" MATCHES "^${DUNE_MODULE_NAME_PATTERN}$")
    set(${ARG_RESULT} TRUE PARENT_SCOPE)
  else()
    set(${ARG_RESULT} FALSE PARENT_SCOPE)
    if(ARG_STRICT)
      message(FATAL_ERROR "Invalid DUNE module name: '${ARG_NAME}'")
    endif()
  endif()

endfunction()


function(dune_check_module_version)

  dune_parse_function_arguments(
    flags STRICT
    single VERSION RESULT
    required VERSION
    defaults
      RESULT VERSION_OK
    )

  if("${ARG_VERSION}" MATCHES "^${DUNE_BASIC_VERSION_PATTERN}$")
    set(${ARG_RESULT} TRUE PARENT_SCOPE)
  else()
    set(${ARG_RESULT} FALSE PARENT_SCOPE)
    if(ARG_STRICT)
      message(FATAL_ERROR "Invalid DUNE module version: '${ARG_VERSION}'")
    endif()
  endif()

endfunction()



function(dune_parse_module_file)

  dune_parse_function_arguments(
    flags STORE_GLOBAL
    single PATH NAME_VAR
    defaults
      PATH "${CMAKE_CURRENT_SOURCE_DIR}"
    )

  # Parse well-known entries
  dune_parse_module_file_entry(PATH "${ARG_PATH}" KEY Module)
  dune_parse_module_file_entry(PATH "${ARG_PATH}" KEY Version)
  dune_parse_module_file_entry(PATH "${ARG_PATH}" KEY Maintainer OPTIONAL)
  dune_parse_module_file_entry(PATH "${ARG_PATH}" KEY Description OPTIONAL)
  dune_parse_module_file_entry(PATH "${ARG_PATH}" KEY Repository OPTIONAL)
  dune_parse_module_file_entry(PATH "${ARG_PATH}" KEY Depends OPTIONAL)
  dune_parse_module_file_entry(PATH "${ARG_PATH}" KEY Suggests OPTIONAL)
  dune_parse_module_file_entry(PATH "${ARG_PATH}" KEY Group DEFAULT ${module})

  dune_check_module_name(NAME ${module} STRICT)
  dune_check_module_version(VERSION "${version}" STRICT)

  if(depends)
    dune_parse_module_spec_list(MODULES "${depends}")

    if(NOT SPEC_OK)
      message(FATAL_ERROR "${ARG_PATH}: Invalid syntax in dependencies list: ${depends}")
    endif()

    foreach(dep IN LISTS SPEC_NAMES)
      list(APPEND dep_types REQUIRED)
    endforeach()

    set(dep_names "${SPEC_NAMES}")
    set(dep_versions "${SPEC_VERSIONS}")
    string(REPLACE ";" "\\;" dep_components "${SPEC_COMPONENTS}")
    set(dep_components "${SPEC_COMPONENTS}")

  endif()

  if(suggests)
    dune_parse_module_spec_list(MODULES "${suggests}")

    if(NOT SPEC_OK)
      message(FATAL_ERROR "${ARG_PATH}: Invalid syntax in suggestions list: ${suggests}")
    endif()

    foreach(dep IN LISTS SPEC_NAMES)
      list(APPEND dep_types OPTIONAL)
    endforeach()

    list(APPEND dep_names "${SPEC_NAMES}")
    list(APPEND dep_versions "${SPEC_VERSIONS}")
    string(REPLACE ";" ";" components "${SPEC_COMPONENTS}")
    list(APPEND dep_components "${components}")

  endif()

  unset(deps)
  dune_list_indices(LIST dep_names VAR dep_indices)
  foreach(i IN LISTS dep_indices)
    list(GET dep_names ${i} dep_name)
    list(GET dep_versions ${i} dep_version)
    list(GET dep_types ${i} dep_type)
    list(GET dep_components ${i} dep_component)
    if("${dep_component}" STREQUAL NOTFOUND)
      set(dep_component "")
    endif()
    string(REPLACE ";" "\\\\;" dep_component "${dep_component}")

    list(APPEND deps "${dep_name}\\;${dep_version}\\;${dep_type}\\;${dep_component}")
  endforeach()

  # store location as a global variable
  set(DUNE_${module}_PROJECT_DIR "${ARG_PATH}" CACHE PATH "source root of ${module}" FORCE)

  # We have to specify the project directory here because the CMake project might not have been created yet
  dune_set_project_property(PROJECT ${module} NAME PROJECT_NAME VALUE "${module}")
  dune_set_project_property(PROJECT ${module} NAME PROJECT_VERSION VALUE "${version}")
  dune_set_project_property(PROJECT ${module} NAME PROJECT_MAINTAINER VALUE "${maintainer}")
  dune_set_project_property(PROJECT ${module} NAME PROJECT_DESCRIPTION VALUE "${description}")
  dune_set_project_property(PROJECT ${module} NAME PROJECT_REPOSITORY_URL VALUE "${repository}")
  dune_set_project_property(PROJECT ${module} NAME PROJECT_DEPENDENCIES VALUE "${deps}")
  dune_set_project_property(PROJECT ${module} NAME PROJECT_GROUP VALUE "${group}")

  if(DEFINED ARG_NAME_VAR)
    set(${ARG_NAME_VAR} ${module} PARENT_SCOPE)
  endif()

endfunction()
