include_guard(GLOBAL)

include(DuneParseArguments)
include(DuneParseModuleFile)
include(DuneLibrary)
# include(DuneSymlinkOrCopy)
include(GNUInstallDirs)
include(DuneProperties)
include(DuneTestMacros)
include(DuneFinalize)

# include(DunePkgConfig)

option(DUNE_BUILDSYSTEM_COMPAT_MODE "Turn on support for the old CMake build system")

macro(dune_find_dependencies)

  dune_get_project_property(NAME PROJECT_DEPENDENCIES VAR _dfd_deps)

  foreach(_dfd_dep IN LISTS _dfd_deps)
    list(GET _dfd_dep 0 _dfd_dep_name)
    list(GET _dfd_dep 1 _dfd_dep_version)
    list(GET _dfd_dep 2 _dfd_dep_required)
    list(GET _dfd_dep 3 _dfd_dep_components)

    # TODO: Version handling!

    set(_dfd_required_components "")
    set(_dfd_optional_components "")

    foreach(_dfd_component IN LISTS _dfd_dep_components)

      list(GET _dfd_component 0 _dfd_comp_name)
      list(GET _dfd_component 1 _dfd_comp_required)

      if(_dfd_comp_required)
        list(APPEND _dfd_required_components ${_dfd_comp_name})
      else()
        list(APPEND _dfd_optional_components ${_dfd_comp_name})
      endif()

    endforeach()

    dune_find_module(
      NAME ${_dfd_dep_name}
      # ${_dfd_dep_version}
      ${_dfd_dep_required}
      COMPONENTS ${_dfd_required_components}
      OPTIONAL_COMPONENTS ${_dfd_optional_components}
      )
  endforeach()

endmacro()


function(dune_project)

  dune_parse_function_arguments(
    unpack_forwarded_arguments
    flags
      UNVERSIONED_HEADERS
      HEADER_ONLY
      DISABLE_CMAKE_EXPORTS
      EXPORT_UNPREFIXED_MACROS
      NO_PROJECT_PREFIX
      DISABLE_DOXYGEN
    single
      HEADER_PREFIX
      CONFIG_HEADER
      CXX_STANDARD
      LIBRARY
    multi
      COMPILE_OPTIONS
      COMPILE_DEFINITIONS
      INCLUDE_SUBDIRECTORIES
      DEFAULT_TEST_LABELS
      LANGUAGES # Was handled by the outer macro, but we still need to accept it here as it gets forwarded
    )

  if(DUNE_BUILDSYSTEM_COMPAT_MODE)
    set(ARG_DISABLE_CMAKE_EXPORTS ON)
  endif()

  dune_get_project_property(NAME COMMON_TEMPLATE_DIR VAR template_dir)

  if(ARG_EXPORT_UNPREFIXED_MACROS)
    dune_set_project_property(NAME EXPORT_UNPREFIXED_MACROS VALUE ON)
  endif()

  # make current project's CMake modules available
  set(helper ${CMAKE_MODULE_PATH})
  list(APPEND helper "${PROJECT_SOURCE_DIR}/cmake/modules")
  list(APPEND helper "${PROJECT_BINARY_DIR}/cmake/private-modules")
  set(CMAKE_MODULE_PATH ${helper} PARENT_SCOPE)

  # by default, projects are in group dune
  # This mainly causes headers to be installed with a prefix of ${GROUP}-${VERSION}/
  dune_get_project_property(NAME PROJECT_GROUP VAR group)

  string(TOUPPER "${group}_" DUNE_DEFINE_PREFIX)
  dune_set_project_property(NAME PROJECT_DEFINE_PREFIX VALUE ${DUNE_DEFINE_PREFIX})

  if(ARG_UNVERSIONED_HEADERS)
    set(DUNE_HEADER_VERSION_PREFIX "")
  else()
    set(DUNE_HEADER_VERSION_PREFIX "${group}-${PROJECT_VERSION}/")
  endif()

  if("${ARG_HEADER_PREFIX}" STREQUAL "")
    string(TOLOWER "${PROJECT_NAME}" ARG_HEADER_PREFIX)
    string(REPLACE "-" "/" ARG_HEADER_PREFIX "${ARG_HEADER_PREFIX}")
    # get rid of potential trailing slash
    string(REGEX REPLACE "/$" "" ARG_HEADER_PREFIX "${ARG_HEADER_PREFIX}")
  endif()

  set(DUNE_GROUP "${group}")
  set(DUNE_HEADER_PREFIX "${ARG_HEADER_PREFIX}")

  #set(DUNE_HEADER_VERSION_PREFIX "${DUNE_HEADER_VERSION_PREFIX}" PARENT_SCOPE)
  #set(DUNE_GROUP "${DUNE_GROUP}" PARENT_SCOPE)
  #set(DUNE_HEADER_PREFIX "${DUNE_HEADER_PREFIX}" PARENT_SCOPE)

  # by default, the config header is config.hh with the header prefix prepended
  if("${ARG_CONFIG_HEADER}" STREQUAL "")
    set(ARG_CONFIG_HEADER config.hh)
  endif()

  set(DUNE_CONFIG_HEADER "${DUNE_HEADER_PREFIX}/${ARG_CONFIG_HEADER}")
  dune_set_project_property(NAME PROJECT_CONFIG_HEADER VALUE "${DUNE_CONFIG_HEADER}")

  if(ARG_HEADER_ONLY)
    set(library_type INTERFACE)
    set(main_library_link_type INTERFACE)
  else()
    set(main_library_link_type PUBLIC)
  endif()

  # set(DUNE_MAIN_LIBRARY_LINK_TYPE ${DUNE_MAIN_LIBRARY_LINK_TYPE} PARENT_SCOPE)

  if(DUNE_BUILDSYSTEM_COMPAT_MODE)
    set(main_library_compat_mode MAIN_LIBRARY_COMPAT_MODE)
  endif()

  if (ARG_CXX_STANDARD)
    set(cxx_standard_arg CXX_STANDARD ${ARG_CXX_STANDARD})

    # make CMake aware of this change if the variable has not yet been set explicitly
    if(NOT CMAKE_CXX_STANDARD GREATER ARG_CXX_STANDARD)
      set(CMAKE_CXX_STANDARD_REQUIRED ON)
      set(CMAKE_CXX_STANDARD_REQUIRED ON PARENT_SCOPE)
      set(CMAKE_CXX_STANDARD ${ARG_CXX_STANDARD})
      set(CMAKE_CXX_STANDARD ${ARG_CXX_STANDARD} PARENT_SCOPE)

      # turn off compiler extensions
      set(CMAKE_CXX_EXTENSIONS OFF)
      set(CMAKE_CXX_EXTENSIONS OFF PARENT_SCOPE)
    endif()
  endif()

  if(ARG_LIBRARY)
    set(library_name "NAME ${ARG_LIBRARY}")
  endif()

  if(ARG_NO_PROJECT_PREFIX)
    set(no_project_prefix NO_PROJECT_PREFIX)
  endif()

  # dune_deprecate_variable(VAR DUNE_MOD_NAME_UPPERCASE VERSION 2.6 HINT "Use PROJECT_VERSION instead.")

  # the project version must be set before adding the library; otherwise the automatically
  # generated code for the version function will not compile
  dune_add_library(
    ${library_name}
    MAIN_LIBRARY
    ${library_type}
    ${main_library_compat_mode}
    ${no_project_prefix}
    ${cxx_standard_arg}
    COMPILE_OPTIONS ${ARG_COMPILE_OPTIONS}
    COMPILE_DEFINITIONS ${ARG_COMPILE_DEFINITIONS}
    INCLUDE_SUBDIRECTORIES ${ARG_INCLUDE_SUBDIRECTORIES}
    )

  foreach(module IN LISTS ${PROJECT_NAME}_DEPENDS)
    dune_split_module_version("${module}" module_name module_version)
    if ("${module_version}" STREQUAL "")
      set(module_version_specifier "")
    else()
      string(REGEX REPLACE "[^0-9]*([0-9].*)" "\\1" module_version "${module_version}")
      dune_extract_major_minor_version("${module_version}" module_version)
      set(module_version_specifier "VERSION ${module_version_MAJOR}.${module_version_MINOR}.${module_version_REVISION}")
    endif()

    dune_set_project_property(PROPERTY MODULE_FILE_DEPENDENCIES APPEND VALUE "${module_name}##${module_version_specifier}##REQUIRED")

  endforeach()

  foreach(module IN LISTS ${PROJECT_NAME}_SUGGESTS)
    dune_split_module_version("${module}" module_name module_version)

    if ("${module_version}" STREQUAL "")
      set(module_version_specifier "")
    else()
      dune_extract_major_minor_version("${module_version}" module_version)
      set(module_version_specifier "VERSION ${MODULE_VERSION_MAJOR}.${MODULE_VERSION_MINOR}.${MODULE_VERSION_REVISION}")
    endif()

    dune_set_project_property(PROPERTY MODULE_FILE_DEPENDENCIES APPEND VALUE "${module_name}##${module_version_specifier}##")

  endforeach()

  if(NOT ARG_DEFAULT_TEST_LABELS)
    set(ARG_DEFAULT_TEST_LABELS standard)
  endif()

  dune_set_project_property(NAME PROJECT_DEFAULT_TEST_LABELS VALUE ${ARG_DEFAULT_TEST_LABELS})

  dune_enable_testing()
  dune_declare_test_labels(LABELS ${ARG_DEFAULT_TEST_LABELS})

  if(ARG_DISABLE_DOXYGEN)
    dune_set_project_property(NAME PROJECT_BUILD_DOXYGEN VALUE OFF)
  else()
    dune_set_project_property(NAME PROJECT_BUILD_DOXYGEN VALUE ON)
  endif()

  dune_register_finalizer(CODE "_dune_finalize_project()")

endfunction()


macro(dune_find_module)

  cmake_parse_arguments(
    __ARG
    "REQUIRED;RECOMMENDED;OPTIONAL"
    "NAME;VERSION"
    "COMPONENTS;OPTIONAL_COMPONENTS"
    "${ARGN}"
    )

  if(__ARG_REQUIRED)
    set(__dependency_type REQUIRED)
  else()
    # we need to reset the variable because we are inside a macro, not a function
    unset(__dependency_type)
  endif()

  find_package(
    ${__ARG_NAME}
    ${__dependency_type}
    COMPONENTS ${__ARG_COMPONENTS}
    OPTIONAL_COMPONENTS ${__ARG_OPTIONAL_COMPONENTS}
    CONFIG
    )

endmacro()


function(dune_add_cmake_modules)

  cmake_parse_arguments(
    ARG
    "NO_AUTO_INCLUDE"
    ""
    "MODULES"
    "${ARGN}"
    )

  install(
    FILES ${ARG_MODULES}
    DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}-${PROJECT_VERSION}/modules"
    COMPONENT devel
    )

  if(ARG_NO_AUTO_INCLUDE)
    return()
  endif()

  set(modules "")

  foreach(module IN LISTS ARG_MODULES)
    string(REGEX REPLACE "\\.cmake$" "" module_name "${module}")
    list(APPEND modules "${module_name}")
  endforeach()

  dune_set_project_property(PROPERTY CMAKE_MODULES APPEND VALUE ${modules})

endfunction()


function(dune_add_config_cmake_code)

  cmake_parse_arguments(
    ARG
    ""
    ""
    "CODE"
    "${ARGN}"
    )

  dune_set_project_property(PROPERTY CMAKE_CONFIG_CMAKE_CODE APPEND_STRING VALUE "\n${ARG_CODE}\n")

endfunction()


function(dune_add_executable)

  cmake_parse_arguments(
    ARG
    "SYSTEM_FROM_IMPORTED;EXCLUDE_FROM_ALL"
    "NAME"
    "SOURCES;LINK_LIBRARIES;LIBRARIES;COMPILE_DEFINITIONS;COMPILE_OPTIONS;CMAKE_GUARD"
    "${ARGN}"
    )

  dune_evaluate_boolean(
    RESULT
      guard
    CONDITIONS
      ${ARG_CMAKE_GUARD}
    )

  if(NOT guard)
    return()
  endif()

  list(LENGTH ARG_SOURCES sources_count)

  if(sources_count LESS 1)
    message(SEND_ERROR "dune_add_executable: Need at least one source file for target ${ARG_NAME}")
  endif()

  if(NOT DEFINED ARG_NAME)
    if(sources_count GREATER 1)
      message(SEND_ERROR "dune_add_executable: Need exactly one source file when omitting name (sources: ${ARG_SOURCES})")
    endif()

    list(GET ARG_SOURCES 0 ARG_NAME)
    string(REGEX REPLACE "\\.[A-Za-z]+$" "" ARG_NAME "${ARG_NAME}")
  endif()

  if(ARG_EXCLUDE_FROM_ALL)
    set(exclude_from_all EXCLUDE_FROM_ALL)
  endif()

  add_executable(
    ${ARG_NAME}
    ${exclude_from_all}
    ${ARG_SOURCES}
    )

  if(NOT ARG_SYSTEM_FROM_IMPORTED)
    set_target_properties(
      ${ARG_NAME}
      PROPERTIES
        NO_SYSTEM_FROM_IMPORTED ON
      )

  endif()

  if(DEFINED ARG_LINK_LIBRARIES)
    message(WARNING "Don't use LINK_LIBRARIES, ignoring option")
  endif()

  dune_get_project_property(PROPERTY CMAKE_EXPORTED_LIBRARIES VAR default_libs)

  target_link_libraries(
    ${ARG_NAME}
    PUBLIC
      ${default_libs}
      ${ARG_LIBRARIES}
    )

  target_compile_definitions(
    ${ARG_NAME}
    PUBLIC
      ${ARG_COMPILE_DEFINITIONS}
    )

  # make sure the good old HAVE_CONFIG_H magic keeps working
  target_compile_definitions(
    ${ARG_NAME}
    PRIVATE
      HAVE_CONFIG_H
    )

  target_include_directories(${ARG_NAME} PRIVATE "${PROJECT_BINARY_DIR}/private")

endfunction()


function(_dune_finalize_project)

  include(CMakePackageConfigHelpers)

  dune_get_project_property(NAME PROJECT_CMAKE_MODULES VAR DUNE_CMAKE_MODULES)
  dune_get_project_property(NAME PROJECT_LIBRARIES VAR DUNE_CMAKE_LIBRARIES)
  dune_get_project_property(NAME PROJECT_EXPORTED_LIBRARIES VAR DUNE_CMAKE_EXPORTED_LIBRARIES)
  dune_get_project_property(NAME PROJECT_EXPORTED_VARIABLES VAR DUNE_CMAKE_EXPORTED_VARIABLES)
  dune_get_project_property(NAME PROJECT_EXPORTED_DEFINES VAR DUNE_CMAKE_EXPORTED_DEFINES)
  dune_get_project_property(NAME PROJECT_EXTERNAL_LIBRARIES)
  dune_get_project_property(NAME PROJECT_FOUND_COMPONENTS VAR DUNE_CMAKE_FOUND_COMPONENTS)
  dune_get_project_property(NAME PROJECT_COMPONENTS_WITH_DESCRIPTION VAR DUNE_CMAKE_COMPONENTS_WITH_DESCRIPTION)
  dune_get_project_property(NAME PROJECT_DEPENDENCIES)
  dune_get_project_property(NAME PROJECT_EXPORTED_PRIVATE_INCLUDES VAR DUNE_CMAKE_EXPORTED_PRIVATE_INCLUDES)
  dune_get_project_property(NAME PROJECT_EXPORTED_INCLUDES VAR DUNE_CMAKE_EXPORTED_INCLUDES)
  dune_get_project_property(NAME PROJECT_CMAKE_CONFIG_CODE)
  dune_get_project_property(NAME PROJECT_EARLY_CMAKE_CONFIG_CODE)
  dune_get_project_property(NAME PROJECT_UNCONDITIONAL_CMAKE_CONFIG_CODE)
  #dune_get_project_property(PROPERTY CMAKE_GRIDTYPES VAR DUNE_CMAKE_GRIDTYPES)
  #dune_get_project_property(PROPERTY CMAKE_EXPORTED_GRIDTYPES VAR )
  dune_get_project_property(NAME CMAKE_EXPORTED_IMPORTED_EXECUTABLES VAR DUNE_CMAKE_EXPORTED_IMPORTED_EXECUTABLES)
  dune_get_project_property(NAME BUILD_DOXYGEN VAR build_doxygen)
  dune_get_project_property(NAME PROJECT_CONFIG_HEADER VAR DUNE_CONFIG_HEADER)
  dune_get_property(SCOPE GLOBAL NAME COMMON_TEMPLATE_DIR VAR template_dir)

  dune_mangle_symbol(SYMBOL ${PROJECT_NAME} UPPERCASE VAR dune_project_c_symbol)

  dune_get_project_property(NAME PROJECT_MAIN_LIBRARY)

  get_property(main_library_type TARGET ${dune_project_main_library} PROPERTY TYPE)

  if(${main_library_type} STREQUAL INTERFACE_LIBRARY)
    set(main_library_link_type INTERFACE)
  else()
    set(main_library_link_type PUBLIC)
  endif()

  foreach(dep IN LISTS dune_project_dependencies)
    list(GET dep 0 dep)

    if(${dep}_FOUND)

      # if(${__ARG_NAME}_FOUND)
      #  dune_set_project_property(PROPERTY CMAKE_DEPENDENCIES APPEND VALUE "${__ARG_NAME}##${__dependency_type}##${__ARG_COMPONENTS}")
      #  dune_set_project_property(PROPERTY MODULE_LIBRARIES APPEND VALUE ${${__ARG_NAME}_LIBRARIES})
      # endif()

      target_link_libraries(
        ${dune_project_main_library}
        ${main_library_link_type}
        ${${dep}_LIBRARIES}
        )

    endif()

  endforeach()


  if(build_doxygen)
    dune_build_doxygen()
  endif()

  list(JOIN DUNE_CMAKE_FOUND_COMPONENTS " " component_line)
  if(NOT "${component_line}" STREQUAL "")
    set(component_line "\\n      components: ${component_line}")
  endif()

  set(DUNE_CMAKE_BUILD_DIR_PACKAGE_DESCRIPTION "non-installed version
      version   : ${PROJECT_VERSION}
      build dir : ${PROJECT_BINARY_DIR}
      source dir: ${PROJECT_SOURCE_DIR}${component_line}"
    )

  # use ${PACKAGE_PREFIX_DIR} here to retain relocatability
  set(DUNE_CMAKE_INSTALLED_PACKAGE_DESCRIPTION "installed version
      version   : ${PROJECT_VERSION}
      prefix    : \${PACKAGE_PREFIX_DIR}${component_line}"
    )

  # use version string where a possible "-git" is replaced by ".git" to make the
  # package version file work
  string(REPLACE "-" "." version ${PROJECT_VERSION})

  write_basic_package_version_file(
    cmake/${PROJECT_NAME}-config-version.cmake
    VERSION ${version}
    COMPATIBILITY ExactVersion
    )

  if(DUNE_BUILDSYSTEM_COMPAT_MODE)
    set(MODULE_SPECIFIC_CONFIG "")
  else()
    if(EXISTS config.cmake.in)
      file(READ config.cmake.in module_config)
      string(CONFIGURE "${module_config}" MODULE_SPECIFIC_CONFIG @ONLY)
    endif()
  endif()

  string(CONFIGURE "${dune_project_unconditional_cmake_config_code}" dune_project_unconditional_cmake_config_code @ONLY)
  string(CONFIGURE "${dune_project_early_cmake_config_code}" dune_project_early_cmake_config_code @ONLY)
  string(CONFIGURE "${dune_project_cmake_config_code}" dune_project_cmake_config_code @ONLY)

  configure_package_config_file(
    "${template_dir}/config.cmake.in"
    cmake/${PROJECT_NAME}-config.cmake
    INSTALL_DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}-${PROJECT_VERSION}
    PATH_VARS
    CMAKE_INSTALL_PREFIX
    CMAKE_INSTALL_LIBDIR
    NO_CHECK_REQUIRED_COMPONENTS_MACRO
    )

  install(
    FILES
      ${PROJECT_BINARY_DIR}/cmake/${PROJECT_NAME}-config-version.cmake
      ${PROJECT_BINARY_DIR}/cmake/${PROJECT_NAME}-config.cmake
    DESTINATION
      ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}-${PROJECT_VERSION}
    COMPONENT
      devel
    )

  if (DUNE_BUILDSYSTEM_COMPAT_MODE)
    file(READ "${PROJECT_SOURCE_DIR}/config.h.cmake" module_config_h)
  else()
    if(EXISTS "${PROJECT_SOURCE_DIR}/config.h.in")
      file(READ "${PROJECT_SOURCE_DIR}/config.h.in" module_config_h)
    endif()
  endif()

  string(CONFIGURE "${module_config_h}" MODULE_SPECIFIC_DEFINES @ONLY)

  set(upstream_config_headers "")
  foreach(dep IN LISTS dune_project_dependencies)
    list(GET dep 0 dep) # extract name
    if (${dep}_FOUND)
      string(APPEND upstream_config_headers "#include <${${dep}_CONFIG_HEADER}>\n")
    else()
      string(APPEND upstream_config_headers "// #include <${dep}_CONFIG_HEADER> (module ${dep} not found)\n")
    endif()
  endforeach()

  set(exported_config_headers "")
  foreach(header IN LISTS DUNE_CMAKE_EXPORTED_INCLUDES)
    string(REGEX REPLACE "(.+)##(.*)" "\\1" header_name "${header}")
    string(REGEX REPLACE "(.+)##(.*)" "\\2" header_description "${header}")
    if(header_description)
      string(APPEND exported_config_headers "\n// ${header_description}")
    endif()
    string(APPEND exported_config_headers "\n#include <${header_name}>\n")
  endforeach()

  set(config_results "\n")
  foreach(result IN LISTS DUNE_CMAKE_EXPORTED_DEFINES)
    string(REGEX REPLACE "(.+)##(.*)" "\\1" result_define "${result}")
    string(REGEX REPLACE "(.+)##(.*)" "\\2" result_description "${result}")
    string(APPEND config_results "\n/* ${result_description} */\n${result_define}\n")
  endforeach()
  string(CONFIGURE "${config_results}" CONFIGURATION_RESULTS @ONLY)

  configure_file(
    "${template_dir}/config.h.in"
    "${DUNE_CONFIG_HEADER}"
    @ONLY
    )

  get_filename_component(dune_config_header_dir "${DUNE_CONFIG_HEADER}" DIRECTORY)

  install(
    FILES "${PROJECT_BINARY_DIR}/${DUNE_CONFIG_HEADER}"
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${DUNE_HEADER_VERSION_PREFIX}${dune_config_header_dir}"
    COMPONENT devel
    )

  set(exported_private_config_headers "")
  foreach(header IN LISTS DUNE_CMAKE_EXPORTED_PRIVATE_INCLUDES)
    string(REGEX REPLACE "(.+)##(.*)" "\\1" header_name "${header}")
    string(REGEX REPLACE "(.+)##(.*)" "\\2" header_description "${header}")
    if(header_description)
      string(APPEND exported_private_config_headers "\n// ${header_description}")
    endif()
    string(APPEND exported_private_config_headers "\n#include <${header_name}>\n")
  endforeach()

  configure_file(
    "${template_dir}/private-config.h.in"
    "${PROJECT_BINARY_DIR}/private/config.h"
    @ONLY
    )

  if(DUNE_SYMLINK_TO_SOURCE_TREE)
    dune_symlink_to_source_tree()
  endif()

  # get_property(install_programs_export GLOBAL PROPERTY DUNE_INSTALL_${PROJECT_NAME}-programs)

  if(install_programs_export)

    list(SORT install_programs_export)
    list(REMOVE_DUPLICATES install_programs_export)

    foreach(component IN LISTS install_programs_export)

      set(export_set "${PROJECT_NAME}-${component}-programs")

      install(
        EXPORT ${export_set}
        DESTINATION "lib/cmake/${PROJECT_NAME}-${PROJECT_VERSION}"
        COMPONENT ${component}
        )

      export(
        EXPORT ${export_set}
        FILE "cmake/${export_set}.cmake"
        )

    endforeach()

  endif()

  # gcreate_and_install_pkconfig("lib")

  if ("$CMAKE_CURRENT_SOURCE_DIR" STREQUAL "${CMAKE_SOURCE_DIR}")
    feature_summary(WHAT ALL)
  endif()

  set(${PROJECT_NAME}_DIR "${CMAKE_CURRENT_BINARY_DIR}/cmake" CACHE PATH "path to directory containing dune-common-config.cmake")

endfunction()


function(dune_install_headers)

  dune_parse_function_arguments(
    flags ABSOLUTE_INPUT_PATHS
    single DESTINATION
    multi HEADERS
    )

  if(ARG_DESTINATION)
    set(destination "${ARG_DESTINATION}")
  else()
    file(RELATIVE_PATH destination "${PROJECT_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}/")
  endif()

  dune_get_project_property(NAME PROJECT_HEADER_VERSION_PREFIX VAR install_prefix)

  include(GNUInstallDirs)

  install(
    FILES
      ${ARG_HEADERS}
    DESTINATION
      "${CMAKE_INSTALL_INCLUDEDIR}/${install_prefix}${destination}"
    COMPONENT
      devel
    )

  if(ARG_ABSOLUTE_INPUT_PATHS)
    set(prefix "")
  else()
    set(prefix "${CMAKE_CURRENT_SOURCE_DIR}/")
  endif()

  foreach(header IN LISTS ARG_HEADERS)
    dune_set_project_property(NAME PROJECT_DOCUMENTED_HEADERS MODE APPEND VALUE "${prefix}${header}")
  endforeach()

  if ("${ARG_HEADERS}" AND NOT ARG_ABSOLUTE_INPUT_PATHS)
    dune_set_project_property(NAME PROJECT_DOXYGEN_INPUT_DIRECTORIES MODE APPEND VALUE "${CMAKE_CURRENT_SOURCE_DIR}")
  endif()

endfunction()


function(dune_export_variable)

  cmake_parse_arguments(
    ARG
    "NO_PREFIX"
    "NAME"
    "VALUE"
    "${ARGN}"
    )

  if(NOT DEFINED ARG_VALUE)
    set(ARG_VALUE ${${ARG_NAME}})
  endif()

  if(ARG_NO_PREFIX)
    set(prefix "")
  else()
    dune_get_project_property(PROPERTY DEFINE_PREFIX VAR prefix)
  endif()

  set(${ARG_NAME} "${ARG_VALUE}" PARENT_SCOPE)
  set(${prefix}${ARG_NAME} "${ARG_VALUE}" PARENT_SCOPE)

  dune_deprecate_variable(
    VAR ${ARG_NAME}
    VERSION 2.7
    HINT "Use the fully qualified ${prefix}${ARG_NAME} instead."
    )

  string(REGEX REPLACE ";" "\\\\;" escaped_value "${ARG_VALUE}")

  dune_set_project_property(PROPERTY CMAKE_EXPORTED_VARIABLES APPEND VALUE "${prefix}${ARG_NAME}##${escaped_value}")

endfunction()


function(dune_export_define)

  cmake_parse_arguments(
    ARG
    "NO_PREFIX;PRIVATE;EXPORT_CMAKE_VARIABLE;NO_CONDITION"
    "NAME;VALUE;DESCRIPTION"
    "RESULT"
    "${ARGN}"
    )

  # catch double declaration of same define within this module
  dune_get_project_property(PROPERTY CMAKE_DECLARED_DEFINES VAR declared_defines)
  if(";${declared_defines};" MATCHES ";${ARG_NAME};")
    message(FATAL_ERROR "Multiple declaration of C++ macro ${ARG_NAME}")
  endif()
  dune_set_project_property(PROPERTY CMAKE_DECLARED_DEFINES APPEND VALUE ${ARG_NAME})

  if(NOT DEFINED ARG_VALUE)
    set(ARG_VALUE 1)
  endif()

  if(NOT DEFINED ARG_DESCRIPTION)
    set(ARG_DESCRIPTION "Result of CMake configuration test for ${ARG_NAME}")
  endif()

  if(ARG_NO_PREFIX OR ARG_PRIVATE)
    set(prefix "")
  else()
    dune_get_project_property(PROPERTY DEFINE_PREFIX VAR define_prefix)
    string(LENGTH ${define_prefix} prefix_length)
    string(SUBSTRING ${ARG_NAME} 0 ${prefix_length} input_prefix)
    if(NOT ${input_prefix} STREQUAL ${define_prefix})
      set(prefix "${define_prefix}")
    endif()
  endif()

  if(NO_CONDITION)
    set(ARG_RESULT 1)
  else()
    if(NOT DEFINED ARG_RESULT)
      set(ARG_RESULT ${${ARG_NAME}})
    endif()
  endif()

  dune_evaluate_boolean(
    RESULT enabled
    CONDITIONS ${ARG_RESULT}
    )

  if(enabled)

    string(REGEX REPLACE ";" "\\\\;" escaped_value "${ARG_VALUE}")
    string(REGEX REPLACE ";" "\\\\;" escaped_description "${ARG_DESCRIPTION}")

    if(ARG_PRIVATE)
      dune_get_project_property(PROPERTY MAIN_LIBRARY VAR main_library)
      target_compile_definitions(${main_library} PRIVATE ${ARG_NAME}=${ARG_VALUE})
    else()
      if(ARG_EXPORT_CMAKE_VARIABLE)
        set(${prefix}${ARG_NAME} "${ARG_VALUE}" PARENT_SCOPE)
        dune_set_project_property(PROPERTY CMAKE_EXPORTED_VARIABLES APPEND VALUE "${prefix}${ARG_NAME}##${escaped_value}")
      endif()
      dune_set_project_property(PROPERTY CMAKE_EXPORTED_DEFINES APPEND VALUE "#define ${prefix}${ARG_NAME} ${escaped_value}##${escaped_description}")
      if(DUNE_EXPORT_UNPREFIXED_MACROS AND NOT ${prefix} STREQUAL "")
        dune_set_project_property(PROPERTY CMAKE_EXPORTED_DEFINES APPEND VALUE "#define ${ARG_NAME} ${escaped_value}##DEPRECATED, USE ${prefix}${ARG_NAME} INSTEAD -- ${escaped_description}")
      endif()
    endif()
  else()
    if(NOT ARG_PRIVATE)
      dune_set_project_property(PROPERTY CMAKE_EXPORTED_DEFINES APPEND VALUE "/* #undef ${prefix}${ARG_NAME} */##${escaped_description}")
    endif()
  endif()

endfunction()


function(dune_export_include)

  cmake_parse_arguments(
    ARG
    "PRIVATE"
    "HEADER;DESCRIPTION"
    ""
    "${ARGN}"
    )

  if(ARG_PRIVATE)
    dune_set_project_property(PROPERTY CMAKE_EXPORTED_PRIVATE_INCLUDES APPEND VALUE "${ARG_HEADER}##${ARG_DESCRIPTION}")
  else()
    dune_set_project_property(PROPERTY CMAKE_EXPORTED_INCLUDES APPEND VALUE "${ARG_HEADER}##${ARG_DESCRIPTION}")
  endif()

endfunction()



function(dune_install_executable)

  cmake_parse_arguments(
    ARG
    "EXPORT;NO_VERSION_SUFFIX;LIBEXEC;OPTIONAL"
    "NAME;TARGET;DESTINATION;COMPONENT;EXECUTABLE"
    ""
    "${ARGN}"
    )

  if((DEFINED ARG_TARGET) AND (DEFINED ARG_EXECUTABLE))
    message(FATAL_ERROR "It is not allowed to specify both TARGET and EXECUTABLE in dune_install_executable(${ARGV})")
  endif()

  if(ARG_TARGET)
    set(source ${ARG_TARGET})
  else()
    set(source ${ARG_EXECUTABLE})
  endif()

  if(NOT DEFINED ARG_NAME)
    set(ARG_NAME ${source})
  endif()

  if(ARG_NO_VERSION_SUFFIX)
    set(installed_name "${ARG_NAME}")
  else()
    set(installed_name "${ARG_NAME}-${PROJECT_VERSION}")
  endif()

  if(NOT DEFINED ARG_DESTINATION)
    if(ARG_LIBEXEC)
      set(ARG_DESTINATION "${CMAKE_INSTALL_LIBEXECDIR}")
    else()
      set(ARG_DESTINATION "${CMAKE_INSTALL_BINDIR}")
    endif()
  endif()

  if(NOT DEFINED ARG_COMPONENT)
    set(ARG_COMPONENT runtime)
  endif()
  set(export_name "${PROJECT_NAME}-${ARG_COMPONENT}-programs")

  if(ARG_EXPORT)
    set(export EXPORT ${export_name})
    dune_set_project_property(PROPERTY "INSTALL_PROGRAMS" APPEND VALUE ${ARG_COMPONENT})
  endif()

  if(ARG_OPTIONAL)
    set(optional OPTIONAL)
  endif()

  if(ARG_COMPONENT)
    set(component COMPONENT ${ARG_COMPONENT})
  endif()

  if(ARG_TARGET)

    get_target_property(target_name ${ARG_TARGET} RUNTIME_OUTPUT_NAME)
    if(NOT target_name)
      set(target_name ${ARG_TARGET})
    endif()
    if(NOT "${installed_name}" STREQUAL "${target_name}")
      set_target_properties(
        ${ARG_TARGET}
        PROPERTIES
          RUNTIME_OUTPUT_NAME "${installed_name}"
        )

      get_target_property(target_dir ${ARG_TARGET} RUNTIME_OUTPUT_DIRECTORY)
      if(NOT target_dir)
        get_target_property(target_dir ${ARG_TARGET} BINARY_DIR)
      endif()

      add_custom_command(
        TARGET "${ARG_TARGET}"
        POST_BUILD
        COMMAND "${CMAKE_COMMAND}" -E create_symlink "${target_dir}/${installed_name}" "${target_dir}/${target_name}"
        BYPRODUCTS "${target_dir}/${installed_name}"
        COMMENT "Creating symlink ${target_dir}/${installed_name} -> ${target_dir}/${target_name}"
        )

    endif()

    install(
      TARGETS
        "${ARG_TARGET}"
      ${export}
      RUNTIME
      DESTINATION
        "${ARG_DESTINATION}"
      ${component}
      ${optional}
      )

    if(ARG_EXPORT)

      # set_property(
      #   DUNE_INSTALL_${PROJECT_NAME}-programs
      # export(
      #   ${export}
      #   TARGETS
      #     ${ARG_TARGET}
      #   )

    endif()

  else()

    find_program(
      found
      NAMES
        ${ARG_EXECUTABLE}
      PATHS
        "${CMAKE_CURRENT_SOURCE_DIR}"
      NO_DEFAULT_PATH
      )

    if(NOT found)
      message(FATAL_ERROR "Could not find executable ${ARG_EXECUTABLE} in dune_install_executable(${ARGN})")
    endif()

    if(CMAKE_HOST_UNIX)
      set(symlink create_symlink)
    else()
      set(symlink copy)
    endif()

    execute_process(
      COMMAND
        "${CMAKE_COMMAND}" -E ${symlink} "${CMAKE_CURRENT_SOURCE_DIR}/${ARG_EXECUTABLE}" "${CMAKE_CURRENT_BINARY_DIR}/${ARG_NAME}"
      )

    if(NOT "${installed_name}" STREQUAL "${ARG_EXECUTABLE}")

      execute_process(
        COMMAND
          "${CMAKE_COMMAND}" -E ${symlink} "${CMAKE_CURRENT_BINARY_DIR}/${ARG_NAME}" "${CMAKE_CURRENT_BINARY_DIR}/${installed_name}"
        )

    endif()

    if(ARG_EXPORT)

      dune_add_imported_executable(
        NAME
          ${ARG_NAME}
        EXECUTABLE
          "${CMAKE_CURRENT_BINARY_DIR}/${ARG_NAME}"
        COMPONENT
          ${ARG_COMPONENT}
        ${export}
        )

    endif()

    install(
      PROGRAMS
        ${ARG_EXECUTABLE}
      RENAME
        ${installed_name}
      DESTINATION
        "${ARG_DESTINATION}"
      ${component}
      )

  endif()


endfunction()



function(dune_evaluate_boolean)

  dune_parse_function_arguments(
    flags ALL ANY SHORT_CIRCUIT
    single RESULT
    multi CONDITIONS
    required RESULT
    )

  set(eval_type FALSE)

  if(ARG_ALL)
    set(eval_type ALL)
  endif()

  if(ARG_ANY)
    if(eval_type)
      message(FATAL_ERROR "Specified multiple combination types in dune_evaluate_boolean() (ALL and ANY)")
    endif()
    set(eval_type ANY)
  endif()

  if(NOT eval_type)
    set(eval_type ALL)
  endif()

  if(eval_type STREQUAL ALL)
    set(result TRUE)
  else()
    set(result FALSE)
  endif()

  foreach(condition ${ARG_CONDITIONS})
    separate_arguments(condition)
    if(${condition})
      if(eval_type STREQUAL ANY)
        set(result TRUE)
        if(ARG_SHORT_CIRCUIT)
          break()
        endif()
      endif()
    else()
      if(eval_type STREQUAL ALL)
        set(result FALSE)
        if(ARG_SHORT_CIRCUIT)
          break()
        endif()
      endif()
    endif()
  endforeach()

  set(${ARG_RESULT} ${result} PARENT_SCOPE)

endfunction()


function(dune_add_subdirectories)

  dune_parse_function_arguments(
    multi SUBDIRECTORIES CMAKE_GUARD
    )

  dune_evaluate_boolean(
    RESULT
      guard
    CONDITIONS
      ${ARG_CMAKE_GUARD}
    )

  if(guard)
    foreach(subdir ${ARG_SUBDIRECTORIES})
      add_subdirectory(${subdir})
    endforeach()
  endif()

endfunction()


function(add_dune_vc_flags)
endfunction()


function(dune_project_add_component)

  dune_parse_function_arguments(
    single NAME
    multi CMAKE_GUARD
    required NAME
    )

  dune_set_project_property(NAME PROJECT_FOUND_COMPONENTS MODE APPEND VALUE ${ARG_NAME})

endfunction()
