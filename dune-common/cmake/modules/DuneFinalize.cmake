include_guard(GLOBAL)

include(DuneParseArguments)
include(DuneProperties)

function(dune_register_finalizer)

  dune_parse_function_arguments(
    flags NO_WARN_ON_DUPLICATE EARLY
    single CODE
    required CODE
    )

  if(ARG_EARLY)
    set(prop CMAKE_EARLY_FINALIZERS)
  else()
    set(prop CMAKE_FINALIZERS)
  endif()

  if(NOT ARG_NO_WARN_ON_DUPLICATE)
    dune_get_project_property(NAME ${prop} VAR finalizers)
    list(FIND finalizers "${ARG_CODE}" duplicate)
    if(NOT (duplicate EQUAL -1))
      message(WARNING "Detected duplicate registration of finalizer \"${ARG_CODE}\". This is probably an error!")
    endif()
  endif()

  dune_set_project_property(NAME ${prop} MODE APPEND VALUE "${ARG_CODE}")

endfunction()


function(_dune_write_finalizers_file)

  dune_get_project_property(NAME CMAKE_EARLY_FINALIZERS VAR early_finalizers)
  dune_get_project_property(NAME CMAKE_FINALIZERS VAR main_finalizers)
  dune_get_property(SCOPE GLOBAL NAME COMMON_TEMPLATE_DIR VAR template_dir)

  set(finalizers "")
  foreach(finalizer IN LISTS early_finalizers main_finalizers)
    string(APPEND finalizers "${finalizer}\n")
  endforeach()

  configure_file(
    "${template_dir}/finalizers.cmake.in"
    "${PROJECT_BINARY_DIR}/cmake/private-modules/${PROJECT_NAME}-finalizers.cmake"
    @ONLY
    )

endfunction()

macro(dune_finalize)

  if(${ARGC} GREATER 0)
    message(FATAL_ERROR "dune_finalize() does not accept arguments")
  endif()

  if(NOT "${PROJECT_BINARY_DIR}/cmake/private-modules" IN_LIST CMAKE_MODULE_PATH)
    list(APPEND CMAKE_MODULE_PATH "${PROJECT_BINARY_DIR}/cmake/private-modules")
  endif()

  _dune_write_finalizers_file()
  include(${PROJECT_NAME}-finalizers)

endmacro()
