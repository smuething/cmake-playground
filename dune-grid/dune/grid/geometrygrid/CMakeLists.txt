dune_library_add_headers(
  HEADERS
    backuprestore.hh
    cachedcoordfunction.hh
    capabilities.hh
    cornerstorage.hh
    coordfunction.hh
    coordfunctioncaller.hh
    datahandle.hh
    declaration.hh
    entity.hh
    entityseed.hh
    geometry.hh
    grid.hh
    gridfamily.hh
    gridview.hh
    hostcorners.hh
    identity.hh
    idset.hh
    indexsets.hh
    intersection.hh
    intersectioniterator.hh
    iterator.hh
    persistentcontainer.hh
  )
