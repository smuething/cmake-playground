#include "config.h"

#include <dune/grid/yaspgrid.hh>

int main(int argc, char** argv)
{

  const auto& comm = Dune::MPIHelper::instance(argc,argv).getCollectiveCommunication();

  constexpr int dim = 2;
  auto grid = Dune::YaspGrid<dim>({1.0},{8,8});
  auto gv   = grid.leafGridView();

  for (int i = 0 ; i < comm.size() ; ++i)
  {
    if (i == comm.rank())
    {
      std::cout << "rank: " << i << std::endl;
      for (auto e : elements(gv))
      {
        for (int d = 0 ; d < dim ; ++d)
          std::cout << e.impl().transformingsubiterator().coord(d) << " ";
        std::cout << std::endl;
      }
      std::cout << std::endl;
    }
    comm.barrier();
  }

}
