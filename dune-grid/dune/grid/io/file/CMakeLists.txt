add_subdirectory(amiramesh)
# add_subdirectory(test)
add_subdirectory(dgfparser)
add_subdirectory(vtk)
add_subdirectory(gnuplot)

dune_library_add_headers(
  HEADERS
    amirameshreader.hh
    amirameshwriter.hh
    dgfparser.hh
    gmshreader.hh
    gmshwriter.hh
    gnuplot.hh
    gnuplot/gnuplot.cc
    printgrid.hh
    starcdreader.hh
    vtk.hh
  )
