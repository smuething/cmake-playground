cmake_minimum_required(VERSION 3.11)

find_package(dune-common CONFIG REQUIRED COMPONENTS buildsystem)

project()

add_subdirectory(dune/grid)

dune_finalize()
