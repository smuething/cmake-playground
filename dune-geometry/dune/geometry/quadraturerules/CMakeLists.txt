dune_library_add_headers(
  HEADERS
    compositequadraturerule.hh
    pointquadrature.hh
    simplexquadrature.hh
    tensorproductquadrature.hh
    gauss_imp.hh
    gausslobatto_imp.hh
    jacobi_1_0_imp.hh
    jacobi_2_0_imp.hh
  )

# exclude_from_headercheck(
#   "pointquadrature.hh
#   simplexquadrature.hh
#   genericquadrature.hh
#   gauss_imp.hh
#   gausslobatto_imp.hh
#   jacobi_1_0_imp.hh
#   jacobi_2_0_imp.hh
#   tensorproductquadrature.hh")

#build the library libquadraturerules
dune_library_add_sources(
  SOURCES
    gauss.cc
    jacobi_1_0.cc
    jacobi_2_0.cc
    quadraturerules.cc
    gausslobatto.cc
  )
